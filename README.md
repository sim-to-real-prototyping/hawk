# hawk


## aja alusta näin
`cd projects/hawk`
`conda activate henv`
`tensorboard --logdir=tensorboard_logs --reload_interval=15 --reload_multifile=True --window_title=Dragon --reload_multifile_inactive_secs=3600`
`./run.sh`


Simulate hawk functionality

https://github.com/higgsfield/RL-Adventure-2


# control logic

## vertical

nivelet lukittuna niin, että f1-3 aina samassa hor suunnassa. konetta voi ojata nuolinäppäimillä. 
* eteen -> vähentää etumoottoria vähän ja lisää takamoottoreita samanverran yhteensä kuin etumottoria
* taakse ->  päinvastainen kuin "eteen"
* vasemmalle -> vähentää taka vasenta vähän,  lisää takaoikeaa vähän
* w -> lisää jokaisen moottorin voimaa vähän. Niin, että kokonaisuus samassa tasapainoissa kuin aiemmin
* s -> päinvastainen kuin q
* q -> käännä f3 ja f1 counterclockwise
* e -> päinvastainen kuin q
* a -> f1 counterclockwise ja f3 clockwise
* d -> päinvatainen kuin a


## vertical - horizontal

Testaa ekana vertical ja horizontal

## horizontal

Deep RL hands on p665 käytetty np.sqrt tasoittamaan isojen arvojen vaikutusta

eteen -> f1 ja f3 alas
taakse -> f1 ja f3 ylös
vasemmalle -> lisää oikean takamottorin tehoa ja vähennä vasemman
oikealle -> päinvastoin kuin vasen
w,s,q,e,a ja d -> kuten horizontal



# jatkokehitysideoita

http://ras.papercept.net/images/temp/IROS/files/0559.pdf
* käyttää Matsuoka
** toisin kuin aiemmin, tässä tapauksessa CPG Matsuoka inputti on sensory feedback, eikä nn mallin outputti
* simulaattorina https://www.ode.org/

https://arxiv.org/pdf/1904.02833.pdf
* Hall effect sensor
** used for proximity sensing, positioning, speed detection, and current sensing applications


Central pattern generators for locomotion control in animals and robots: a review
* Olisiko hyötyä lisätä rytmi normi droneen?
* esim kävelyssä vähentää contrl model reaaliaika vaatimuksia kun rytmi jauhaa pienellä delaylla
* interesting biologically inspired locomotion controllers for biped locomotion can also be constructed based on reflexes rather than on CPG
** onko normi malli kuin refleksi? Ja CPG on ajastin


https://arxiv.org/pdf/2001.04059.pdf
* käytti Matsuoka oscillator. 
** https://www.sciencedirect.com/science/article/pii/S1877050913006613
** https://www.cs.cmu.edu/~hgeyer/Teaching/R16-899B/Papers/Matsuoka85BiolCybern.pdf
** voi olla matemaattisesti haastavan näköinen, mutta vaikuttaa silti yksinkertaiselle
* käärmeen niveliä käsitellään paineen avulla (pneumatically actuated)
** sisältää extensorin ja flexorin
* suhteellisen suoraviivainen
* ppo
* käyttää curriculum learningiä
* käyttää  domain randomization technique
* käyttää Mocap tekniikkaa


https://arxiv.org/pdf/2103.00928.pdf
* vähän sekava.
* käytti fourieria jotenkin.
* residual physics
* PPO. However, powerful policy optimization algorithms such as PPO [2] cannot take full advantage of existing solutions. To close this gap,
** PPO extended
* studies on animals. In these approaches, a set of oscillators are coupled together in a specific manner to generate rhythmic locomotion
* In the scope of this work, data augmentation consists in artificially creating samples by applying a symmetric transformation to actually experienced samples
** eli jos symmetrinen object, niin jokainen liike yhteen suuntaan voidaan soveltaa myös vastaavaksi liikkeeksi toiseen suuntaan? Eli periaatteessa tuplataan samplejen määrä
* Symmetric networks solutions enforce symmetry constraints directly on the policy, by modifying the network architecture. 
* the symmetry loss is the most advantageous approach because it allows the user to define exactly how important symmetry is in the policy, at any given moment during learning.

https://arxiv.org/pdf/1709.07857.pdf
* The second component of the method is a simple, manually designed servoing function that uses the grasp probabilities predicted by C to choose the motor command vi that will continuously control the robot.
** controller malli ennustaa mikä action ja kovakoodattu logiikka määrittelee mitä kyseinen action tekee
* tämäkin käytti simulation randomizationia
* there are two primary types of methods used for domain adaptation: feature-level, and pixellevel.
** pixel level tarkoittaa realistisempia kuvia ja feature level tarkoittaa, että realistisemmassa kuvassa on sellaiset featuret jotka ovat tärkeitä sekä synteettisessä että oikeassa kuvassa. Esim objectkin sijainti kuvassa ei saa muuttua.
* saa inputissa dataa monesta eri lähteestä. malli yrittää ennustaa mistä lähteestä (domain) data on peräisin. tosin eivät taida käyttää sitä tietoa mihinkään?
* ei ennusta continuous commandia, vaan todennäköisyyksiä millä eri komennot johtaa success tilaan.
* kun konvertoi simulaatio kuvan, niin outputtaa myös maskin robot arm, object,s background. Näin varmistaa ettei semantiikka muutu.

https://www.cs.utexas.edu/~sniekum/classes/RL-F17/papers/GSL.pdf
* Each component of at, ait, is the desired joint angle
*  The process of making the simulator more like the real world is referred to as grounding.
* policy ennustaa oikean ympäristön actionin. Convertteri convertoi actionin simulaattorin actioniksi. Simulaattori ja robotti saavat samoja tiloja
* "GAT Implementation": eli a on halutut nivelasennot ja x on muutos ja muutoksen ennustus toimi paremmin? Ei ennusta oikeasti seuraavia nivelkulmia vaan nivelkiihtyvyyksiä. action converter ottaa (s,a) historian.
*  Previous work has shown that when using a model estimated from data it is better to use shorter action trajectories to avoid overfitting to an inaccurate model. Model kai tässä yhteydessä g, eli on-model malli. 
* monimutkainen malli. Ja ehkä raskas


https://arxiv.org/pdf/1703.02702.pdf
* treenaa agentti ja agentin häiritsijä.
* swimmer hyvin samankaltainen kun snake
* adversary voi esim vaihtaa frictionin tai massan. On tosin ihan vaan nivel forcejakin ja forceja objecktiin.
* trpo policy optimizer
* taisivat käyttää rllib kirjastoa
* θ selects at according to the robot’s configuration in joint space, xt, high level intention commands (e.g., walk forward at 75% of maximum velocity)
** Antaa user input komennon kuten dragon kirjoitushetkellä

https://arxiv.org/pdf/1702.02453.pdf
* Part of the power of UP-OSI is that it can dynamically adapt to changing factors in the environment
* For highly dynamic or contact-rich tasks, learning an accurate dynamic model and control policy would require a large amount of high quality data, which can be difficult to acquire
* model = fyysinen model/simulaattori mode, control policy = nn model. dynamic model = simulaattori model, miten siirtyy tilasta toiseen.
* onpolicy (TRPO)
* PyDart2, python wrapper for DART
* We add a dropout layer for OSI after each hidden layer with a dropout rate of 0.1.
* periaatteessa vaihtoehto randomelle dynamicille?
** The purpose of this comparison is to show that providing the model parameters as input to UP results in a more powerful control policy under a range of dynamic models,


https://www.ri.cmu.edu/wp-content/uploads/2018/01/Arjun_AAAI2018_FinalSubmission.pdf
*  for high-dimensional state spaces, which are common in many real-world problems, it may be difficult for the underlying estimator (e.g., a deep neural network) to vary its policy drastically based on a single phase input variable
* continuous action values [-1,1]
* käärme liikkeeseen tämä voisi olla tosi hyvä
* eri lr actorille ja criticille
* continuous action spaceille paljon isompi neural network. Ongelmakin on vaikeampi, mutta ehkä continuous vaatii isomman nn kuin discrete?
* "Actions were appended to the output of the first hidden layer for the critic networks". Tässäkin tapauksessa siis input rakentuu kahdesta eri haarasta


https://arxiv.org/pdf/1707.02286.pdf
* We also show that learning speed can be improved by explicitly structuring terrains to gradually increase in difficulty
* Curriculum learningiä voisi kokeilla
** data bucketteihin. Ensin treenaa helpommalla sitten toiseksi helpoimmalla ... ja sitten vaikeimmalla.
** eli rewrd tuunaamisen sijaan tuunaa esim taskin vaikeutta.
* PPO:hon lisätty KL
* RNN joka backprogataes viimeisten K yli. Tästä syystä rewardkin lasketaan vain viimeisen K yli + bootstrap.
* taas yksi esimerkki missä mallin inputti on kaksi eri haaraa.
** toinen haara sisältää proprioceptive tietoa joka riippuu vani agentista eikä taskista tai ympäristöstä
* jos quadruped opetteli hyppimään aitojen yli niin se selvisi paremmin parametrien, kuten kitkan muutoksista. Verrattuna tasaisessa maassa etenemiseen. 
* The policy trained on the terrain with gradually increasing difficulty improves faster than the one trained on a stationary terrain.


https://arxiv.org/pdf/1707.01495.pdf
* Universal Value Function Approximators
* Monen mallin sijaan yksi malli monella targetilla 

https://arxiv.org/pdf/1610.03518.pdf
* hauska ajatus: sim1 to sim2 transfer. Onko itsesiassa aika sama kuni mitä tapahtuu randomization approacheissa?
* actinit voi olla ihan erilaisia, jopa eri dimensioisia, simulaattorissa ja robotissa
*  evaluating the performance of adapted policies operating in the real world is typically more expensive than executing those policies, as it might, for example, require instrumentation of the physical world with ground truth sensors.

muuta monimutkainen nivelkulma säätö upper limiteiksi.
https://github.com/bulletphysics/bullet3/pull/2919

DDPG, RDPG, locomotion, dynamics randomization, mpo, Retrace

https://arxiv.org/pdf/1710.06537.pdf
Target angles are specified as relative offsets from the current joint rotations
* katso p4 "The parameters which we randomize include"
* erityisesti damping
* tätä voisi yrittää

https://arxiv.org/pdf/1707.02267.pdf
auxiliary task on vain ennustaa kohde objektin sijaintia näkymässä. Eli ei välttämättä tarvitse antaa rewardia.
" in addition to 2 axillary outputs: the 3D cube position
and 3D gripper position"
ActorCritic käyttämään LSTM

https://arxiv.org/pdf/1804.10332.pdf
* When designing the action space, we choose to use the position control mode of the actuators for safety and ease of learning
* Olisi ehkä hyvä rajoittaa moottorien asentoja erilaisilla kaavoilla
** An alternative action space is the eight desired motor angles. However, in this motor space, many configurations are invalid due to self collisions between body parts. This results in an action space where valid actions are scattered nonconvex regions, which significantly increases the difficulty of learning.
* We design a reward function to encourage faster forward running speed and penalize high energy consumption.
* An episode terminates after 1000 steps or when the simulated Minitaur loses balance: its base tilts more than 0.5 radians
** Yritin vastaavaa, muttei toiminyt. Pitänee yrittää vielä uudelleen jossain kohtaa.
* kaava 3. Sen sijaan että action on input, niin action on esm periodical signal ja policy vain hoitaa tasapainottamisen etc.
* jokaisen robotin osan punnitseminen ja ulottuvuuksien mittaaminen -> saa paremman urdf mallin
* sen sijaan, että malli kertoo halutun seuraavan moottori kulman, niin malli kertoo moottorikulman ja nopeuden. Näistä lasketaan penalty.
* tuunaa simulaattori jokaiselle moottorille niin, että inputti sini aaltoa. Varmita että kyseisen moottorin simulaattori näyttää samoja kulmia kuin oikea moottori. Latency historia (obsevation, latency) -pareista.


Policy kohdassa on tehty internal repsesentation, koska eri rotaatiot antavat saman rewardin. Eli jotta, ei tarvitse oppia jokaista rotatoitua tilaa erikseen. Pitäisikö minunkin tehdä koneesta sisäinen malli joka syötetään sitten gravitaatio suuntaan. Tai mikäli gravitaatio poistetaan niin ehkä sisäinen mallin ~ ulkoinen malli.
* voisin ehkä opettaa tekemään tietyt manoverit alemmassa layerissa. ylempi layer huolehtii missä on ylös/alas ja antaa korkeamman tason käskyt alemmalle lauerille. https://arxiv.org/pdf/1610.05182.pdf
* https://arxiv.org/pdf/2002.07717.pdf

voisinko tehdä energiatila mallin? Sellaisen, että jos moottorit osoittaa samaan suuntaan niin korkeampi energiatila. Samoiten jos lift ja moottorit oikeass suunnassa niin hyvä energia tila
* https://arxiv.org/pdf/2002.07717.pdf


erota konfaukset run.py filestä ini fileen. kuten deep RL hands on kirjassa p756

http://github.com/packtpublishing/Deep-Reinforcement-Learning-Hands-On-Second-Edition

Deep RL hands on p512 xvfb-run komennolla videon kaappaus
* xvfb-run -s "-screen 0 640x640x24 +extension GLX" "


https://www.hindawi.com/journals/tswj/2014/489308/

Lisää moottorin F-rotate komponentti mallin inputiksi.

### Stabiilimpi rakenne:
yksinkertaista rakennetta kolmesta fuselagista kahteen.
Vain edessä moottori. etumoottori tehokkaampi. 
jää helposti jumiin jos keikahtaa tarpeeksi taakse tai eteen

### muuta

residual learning, hands on p698

Deep RL hands on kirjassa p576 suositeltiin tätä artikkelia
https://www.monocilindro.com/2016/06/04/how-to-calculate-tait-bryan-angles-acceleration-and-gyroscope-sensors-signal-fusion/
jos haluaa laskea yaw:n

p.setJointMotorControl2 ehkä POSITION_CONTROL -> TORQUE_CONTROL or VELOCITY_CONTROL would work better.

kokeile eps 1e-3. default eps=1e-08 rl hands on388

voisiko malli oppia fyysisessä laitteesas mitkä ovat oikeat pybullet konfit?

Distributional Maximum a Posteriori Policy Optimization (DMPO) (MPO)
Distributed Distributional Deterministic Policy Gradient (D4PG) (DDPG) - vaikuttaa paremmalle robottikäytössä kuin DMPO

lisää damage simulaatioon. Eli jos törmää niin ominaisuudet huononee. Tai ehkä jos törmää tai käyttäytyy liian huonosti niin penaltyä tai episoden loppu isoon miinukseen.

clip_grad_norm_(model.parameters(), CLIP_GRAD) p322

p344. suurin osa ajasta taitaa mennä simulaattorissa. ehkä tästä ei ole juuri nyt hyötyä

laske KL arvo kuten hands on RL sivulla 298
logita myös gradienttien arvoja kuten sivulla 299

hands on kirja 340. mp ja OpenMP kannattaa laittaa os.environment["OMP_NUM_THREADS"] = "1". antoi 2-3x suorituskyky parannuksen. vaikuttaako mun koodeihin? 

torch share_memory(). tämän voisi kutsua net:lle. Testaa onko muutosta mihinkään. ilmeisesti merkkaa vain jos CPU. p341 

hands on kirja p337&339
sen sijaan että yhdellä koneella kopioisi numpy arrayta env prosessista main prosessiin, voi numpyt tallentaa GPUlle ja välittää tensor referencen child prosessista parent prosessiin. tämä taisi tulla automaattisesti torch mp Queue etc mukana

hands on kirja s332: if your environment is cheap in terms of agent interaction (the env is fastm has low memory footprint, allows parallelization and so on), policy gradient method could be better choise. 

hands on kirja s335 jälkimmäinen kappale. milloin tehdä data parallelization ja milloin graph parallelization.

action_spacen koon voisi tehdä mode kohtaiseksi parametriksi. Sitten joku mapperi joka muuttaa model outputin moottorikomennoiksi. Tällä tavalla output koko pienenisi esim fixed moden kohdalla.


painopiste mahdollisimman paljon moottorien alapuolelle kun horisontaali nousu

thermal soaring https://www.pnas.org/content/113/33/E4877

Jos tarve tehdä sujuvia vaihdoksia yhdestä mallista toiseen, niin treenaa kohde mallia aloittamalla lähtömallin tilasta. 

annan penaltyä jos tekee liian paljon moottorien arvojen muutosta -> ettei riko moottoreita rämpyttämällä edestakaisin.

opeta malli simulator_V1 -> aja maailmassa ja keraa data -> paranna simulaatotira datan avulla simulator_V2 -> paranna mallia simulator_V2:lla -> aja ...

tee ympäristöstä enemmin real world https://github.com/google-research/realworldrl_suite 

3D printtausta: https://www.thingiverse.com/
Fusion 360 suositellaan Deep RL hands on p541
* myös OpenSCAD voi olla kokeilemisen arvoinen.

### Hardware

ultrasonic sensors, kuten maxbotix, voi kertoa missä on maa ja missä taivas.
distance sensor 

MicroPython - https://micropython.org/

inertial measurement unit
digital gyroscope
magnetometers
pressure sensor - karkea korkeusmittari -barometer
earth’s magnetic field käyttäminen asennon mittaamisessa? Luultavasti moottorien magneettikentät häirtsevät tuloksia.
Ehkä kalman fitleriä voisi käyttää syhdistämään eri lähteistä tuleva data? Onko parempi/huonompi kuin opittu malli?
Futaba gyro

GPS kretomaan sijainnin(position). http://www.robotics.stanford.edu/~ang/papers/ijrr10-HelicopterAerobatics.pdf

sonar unit, which measures distance from the ground.

https://www.youtube.com/watch?v=HYEzHX6-fIA
https://sites.google.com/view/dads-skill




TODO: 
normalisoi inputit välille 1-, 1
erota policy ja value
alusta std pieneksi. Vika layer
lisää pieni negatiivinen offsetti std:hen.
vaihda log_std.ext -> softmax
tanh -> relu vertailu isolla networkilla
normalisoi value func output?
kokeile Adam b1 = 0.9
laske gae uudelleen ennen jokaista update iteraatiota.

kokeile tau = 0.9 - veikkaan, ettei hyvä koska niin monta steppiä per episode

Kokeile uudelleen annustaa absoluuttisen arvon sijaan kasvu tai väheneminen



Auxilary task ÄLÄ TEE NYT: 
* tee 2 täysin erillistä settiä state, action.. ja molemmille omat gaet ja ppo:ssa laske molemmat itsenäisesti ja summaa lossit.
* Työläs. 
* ehkä nykyiset apu-lossit voisi olla auxilareja oikealle targetille joka liittyy jotenkin user inputtiin?




 What Matters In On-Policy Reinforcement Learning? A Large-Scale Empirical Study (Paper Explained)
 * policy on vaikeaa tuunata toimimaan, value helppoa
 * policyä voi auttaa toimimaan alustamalla mean nollaksi ja std pieneksi. Tämän voi tehdä tuunaamalla viimeisen layerin parametreistä pieniä (1/100 osa muista)
 * action ja value toimi paremmin kun eri networkkeja.
 * use softplus to transform network output into action standard deviation. Nyt käytän log_std.exp()
 * add negative offset to std input to descrease the inital standard deviation of actions.
 * tanh hyvä jos network ei liian syvä. ehkä relu parempi jos network on syvä
 * value network olisi hyvä olla leveä.
 * policy vähän kapeampi MLP
 * normalisoi inputti välille -1, 1, tai mena = ja std = 1
 * value function normalization saattaa parantaa suorituskykyä
 * Use gae. taidan käyytää jo nyt. ja lambda = 0.9 ei value loss clippingiä - ei kai hirmu tärkeä
 * suffle data ennen kuin teet training minibatchin
 * laske GAE uudelleen uudella value functionilla joka epochin jälkeen. 
 * timestep discount on tärkeä. aloita arvosta 0.99
 * Adam B1 = 0.9. Tune learningrate. lr = 0.0003 on hyvä default
 * regularization ei auttanut paljoa.



 RL kirjassa sivulla 453 alkaen lennokkiesimerkki, missä ennustettiin mihin suuntaan otetaan seuraava askel, ei mikä on absoluuttinen arvo
 RL kirjassa sivulla 559 auxiliary tasks ja value functions. Ehkä voisi hyödyntää
* Voisiko aputaski esim ennustaa miten gravitaation local suunta tulee käyttäytymään?
 * auxiliary task lisätään samaan networkkiin kuin varsinainen task!
 RL kirja Temporal Abstraction via Options
 * koita oppia hyödyllisiä komentosarjoja, joista valita action. 
 ** esim 20 outputttia. Ota softmazilla. Yksi output sisältää esim 5 actionia.
 ** voisiko action sarjat oppia jotenkin hyödyllisesti? Voisin ehkä käsin määrittää ne sarjat
 ** tietyssä statessa voi ottaa vain tietyt optionit. Esoim jos local gravity vasemmalla niin eri vaihtoehdot kuin jos oikealla. Edelleen miten opitaan hyvät optionit? sivulla 463 sanotaan, että voi oppia sivun 459 GVF kaavalla
 *** esim GVF, jos kallellaan niin subtaski on oikaista. Lopulta vertical takeoff on näiden subtaskien mudostama optionit.
 RL kirja p470:
 * valuefunktioon esiasetuksna hyvä init arvo. 
 * aloita helposta rewardista ja tuunaa rewardia pikkuhiljaa vaikeammaksi.
 * Imitation learning. Lähinnä kai tekee tästä supervised learningiä?
 * p471 reward signaalin haun voisi automatisoida.





pitäisikö testiin lisätä ohjaussignaali kuten joku lentämään joystickillä? + muta gravitaatio kiihtyvyys kokonaiskiihtyvyydeksi.
* poista speed inputsta
* muuta gravity vector kokonaiskiihtyvyys vektoriksi
* inputiksi abs(kokonaiskiihtyvyysvektori) - gravitaatio_force? 
** kertoo miten paljon moottorien/putoamisen aiheuttamaa kiihtyvyyttä.
* miten ohjaussignaali
** 2 tilaa: hoovering ja flying
** Hoovering tilassa kai gravity melko helppo, kun kiihtyvyys ei saa koskaan mennä kauas gravitystä.
** flying: lennä eteenpäin samassa asennossa. käänny pitch&yaw suunnassa saumattomasti.
* Voi olla, että perustoiminnot voisi tehdä perus ohajuslogiikalla ja ML vain hienosäätää, mm käyttämällä sensoridataa. Osaa ottaa huomioon mm painon muutokset

ALOITA HELPOLLA: 
* Yrittää nousta verticaalisti.
** ei ohjaussignaalia vaan rewardi minimoimaan poikkeamaa gravitystä. Jos toimii niin lisää ohajussignaali. --DONE
** Jos toimii niin koita saada oppimaan lentämään
** Jos ei toimi niin muuta jatkuva arvo delta arvoksi tai komennoksi (plus,minus, zero).
*** Eli absoluuttisen arvon sijaan malli kertoo mihin suuntaan pitää muuttaa
*** ehkä voi ennustaa muutoksen suuruutta. Esim nopea muutos vs hidas muutos. Eli voi olla jatkossakin continuous
** Jos ei toimi niin tee ohjauslogiikka. Koita saada toimimaan sillä. Sitten koita lisätä PPO ehdottamaan parannuksia.
* lisää multiajo ominaisuus, jotta treenaus nopeutuu ja saan parhaan irti koneesta.

Voisinko käyttää pytorchin Attention mallia hyväksi? Tai testata miten se toimii.

jokaisen moottorin pitää tietää mihin missä suunnassa on alas ja mihin kokonaiskiihtyvyys kiihtyvyys osoittaa. 
anna inputiksi missä suunnassa on alas. Tämän oikea toteuttaminen on vaikeaa mutta kai mahdollista.
auttaisiko jos konvertoi nivelkulmia käyttäen moottorien kulmat aina fuselag2 lokaaliin? Kulmat ja positiot-

Tee moottorien kohdalla niin, että uusi arvo muuttaa vanhaa arvoa uuden arvon suuntaan.

nyt taitaa oppia liian toisiaan noudattelevan actorin ja critikin. Lisää pikkasen randomea ppo losseihin.










TODO:
jos tämäkään ei toimi, niin koita tehdä ohjauslogiikka joka hoitaa säädön. Koita saatko sen toimimaan.
* 3 mottoria ja 3 käskyä sen mukaan missä asennossa kone on. Malli yrittää ennustaa eroa nykyiseen asentoon, jotta 
TAI 
kuten ohajuslogiikan kanssa, määritä tavoitetila ja ennustamiten nykytilaa pitää muuttaa jotta päästään tavoitetilaan.
* esim kallellaan vasemmalle niin lisää taka vasemman moottori nostetta hieman ja vähennä oikean.
* kallellaan eteen. lisää etumoottori nostetta vähän ja vähennä takamoottorin nostetta.
* nousee. vähennä kaikkien moottorien nostetta oikeassa suhteessa. eli jos kiihtyvyys yli gravitaation.

*: 







* Rewrd scaling from TÄMÄ ON ILMEISESTI MERKITTÄVÄ --DONE
* TAI Reward Clipping: The implementation also clips the rewards within a preset range (usually [−5, 5] or [−10, 10]
* Orthogonal initialization (and layer scalin?)
* vaihda relut tanh
* Global Gradient Clipping - tekikö DQN implementaatio tätä. Voisi kopsata koodit sieltä
https://arxiv.org/pdf/2005.12729.pdf


Lisääntyykö exploraus jos:
* kasvatan clip_param?
* kasvatan entropy kerrointa ppo_update funktiossa? Muutin 0.001 -> 0.01. jos oli hyötyä niin aivan marginaalisesti

opeta pelkkä moottorien osoittaminen oikeaan suuntaan ja sen jälkeen lisää pikkuhiljaa muita penaltyja


curricula training - unityssä. 
helpotus - lukitse jompi kumpi nivel.
pienennä gravitaatioita jotta muutokset pienempiä.
gravitaatiosuunnan pystyy saamaan kiihtyvyydessäkin -- ehkä
Käytä kolmikopterina- pistä nivelet lukkoon ja koita hoitaa ohaus moottoreilla.



DONE: laita takamoottori osoittamaan hieman ulospäin, jotta pystyy stabilioimaan pyörimisliikettä moottorien tehoilla.


Näitä piti ehtiä käymään läpi: 
https://www.technik-consulting.eu/en/problem_solving/drone_altimeter.html



https://github.com/bulletphysics/bullet3/issues/1925



kun tarvii olla tehokas datan kansa niin kun varsinaisen prototyypin testauksessa, niin eligibility trace saattaisi auttaa (Reinforced Learning, second edition p317 vika kappale.



priorityt:
- aja treeniä mahdollisimman koko ajan
- lue RL kirja
- ala suunitella ensimämistä lego versiota ohajusjärjestelmineen
- katsele yannic etc ML videoita
- kun simulaattori osaa nousta, tehdä manooverin ja laskeutua, niin ala rakentaa ensimmäistä fyysistä versiota. 
-- voisikohan ekalla lego verisolla testata jo verticalia take offia?

