import pybullet as p
import time
from easydict import EasyDict
from models.modes import Modes

class KeyboardActions:
  chars = [EasyDict(x) for x in [ {"k":"q", "v":113, "n":"counter_mirror"},\
                              {"k": "a", "v":97, "n":"counter"},\
                              {"k": "w", "v":119, "n":"add_power"},\
                              {"k": "s", "v":115, "n":"sub_power"},
                              {"k": "e", "v":101, "n":"clock_mirror"},
                              {"k": "d", "v":100, "n":"clock"},
                              {"k": "f", "v":102, "n":"spin_xz"},
                              {"k": "h", "v":104, "n":"spin_minus_xz"},
                              {"k": "t", "v":116, "n":"spin_yz"},
                              {"k": "g", "v":103, "n":"spin_minus_yz"},
                              {"k": "u", "v":117, "n":"normal_mode"},
                              {"k": "i", "v":105, "n":"fixed_mode"},
                              {"k": "left", "v":65295, "n":"motor_roll_counter"},
                              {"k": "right", "v":65296, "n":"motor_roll_clock"},
                              {"k": "up", "v":65297, "n":"pitch_down"},
                              {"k": "down", "v":65298, "n":"pitch_up"}]]
  @staticmethod
  def __recent_actions(chars_mapping):
    def is_pressed_key(key_nr):
      def is_char(obj):
        return obj.v == key_nr
      return is_char

    actions = []
    count = 0
    keys = p.getKeyboardEvents()
    for k, v in keys.items():
      #if v & p.KEY_WAS_TRIGGERED:
      if v == 1 & p.KEY_IS_DOWN:
        pressed_key = list(filter(is_pressed_key(k), chars_mapping))
        if len(pressed_key) > 0:
          actions.append(pressed_key[0].k)

    return actions

  def list_keyboard_actions():
    keyboard_acts = KeyboardActions.__recent_actions(KeyboardActions.chars)
    
    return keyboard_acts
