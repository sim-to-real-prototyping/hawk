from numpy.core.overrides import set_module
import torch
import numpy as np
from torch.cuda import current_device
import torch.optim as optim
#from pathlib import Path
import math
import torch
import random
from models.ppo.ppo import ActorCritic, PpoOperator
from models.ppo.data_converter import PpoDataConverter
from physics_simulator import Physics
from models.modes import Modes
from easydict import EasyDict
from models.file_handler import AgentFileHandler
import copy


class ModeChangePhaser:
  def __init__(self):
    self.reset()
  
  def reset(self):
    self.val = random.randint(30,100)

  @property
  def value(self):
    return self.val

"""
tee UserAction luokka
* get_action: env_count kokonen vektori arvot 0,1,2
** arvot pitää parsia one hotiksi ja lisätä prediction stateen 
** action arvotaan uudelleen aina 80 välein
* reward laskenta tarvitsee tiedon actionista. anna parametrinä.
"""


class UserActionTrain:
  def __init__(self, env_count):
    self.env_count = env_count
    self.action_cnt = 2
    self.actions = np.random.randint(self.action_cnt, size=(env_count))
    self.prev_action = self.actions.tolist()

  def get_action(self):
    return self.actions.tolist()
  

  



class Agent:
  def __init__(self, env_vars):
    self.env_vars = env_vars
    self.file_handler = AgentFileHandler(env_vars)
    self.device = Agent._get_device()
    self.learning_rate = env_vars.learning_rate

    self.user_action_train = self._get_user_action_train()

    # TODO: Should be easy to combine
    converter_fixed = PpoDataConverter(self.env_vars.info, Modes.fixed_mode)
    converter_normal = PpoDataConverter(self.env_vars.info, Modes.normal_mode)

    self.vertical_actions = self.user_action_train.action_cnt
    self.models = EasyDict({
      Modes.fixed_mode: { "model": None, 
                          "optimizer": None, 
                          "start_step": None, 
                          "data_processor": converter_fixed,
                          "input_size":len(converter_fixed.field_info[Modes.fixed_mode]) + self.vertical_actions,
                          "output_size":7},
      Modes.normal_mode: {"model": None, 
                          "optimizer": None, 
                          "start_step": None, 
                          "data_processor":converter_normal,
                          "input_size":len(converter_normal.field_info[Modes.normal_mode]),
                          "output_size":7}
    })
    self.model = None
    self.optimizer = None

    self._init_models()

    self.mini_batch_size  = env_vars.mini_batch_size #128
    self.ppo_epochs       = env_vars.ppo_epochs

    self._init_current_mode(self.env_vars.default_mode, True)

    self.best_reward_begin = self.file_handler.best_reward(self.current_mode)
    self.info = env_vars.info

    self.mode_counter = 0

    self.mode_phaser = ModeChangePhaser()

    self.prev_env = self._get_empty_env_hist(env_vars.num_envs)

    self.test_run_info = EasyDict({"last_action": 0})

  def _get_user_action_train(self):
    return UserActionTrain(self.env_vars.num_envs)


  @staticmethod
  def _get_empty_env_hist(num_envs):
    return [ {"position": {"prev": [0,0], "second_prev":[0,0]}, "direction": {"prev": [0,0,0], "second_prev":[0,0, 0]}} for a in range(num_envs)] 

  def reset_episode(self):
    self.prev_env = self._get_empty_env_hist(self.env_vars.num_envs)
    self.user_action_train = self._get_user_action_train()
    self.set_model(self.env_vars.default_mode, new_episode = True)

  def _init_current_mode(self, mode, new_episode):
    if not new_episode:
      self.current_mode = mode
      self.train_target_mode = mode
      return mode

    self.current_mode = self.env_vars.default_mode
    self.train_target_mode = self.env_vars.default_mode

    if self.current_mode == Modes.fixed_mode and self.env_vars.train:
      #self.train_target_mode = Modes.fixed_mode
      self.current_mode = Modes.normal_mode

    return self.current_mode

  @property
  def mode_info(self):
    return self.file_handler.mode_info

  def change_mode(self, user_inputs):
    if self.env_vars.train:
      return

    if len(user_inputs) != 1:
      raise Exception("Mode can be changed only with one environment. Currently ", len(user_inputs)," environments.")
    
    user_inputs = user_inputs[0]
    contains_normal_mode = "u" in user_inputs
    contains_fixed_mode = "i" in user_inputs
    if contains_normal_mode and contains_fixed_mode:
      print("Set mode command ignored. Too many set mode commands. Ambiguous which to apply.")
      return
    elif contains_normal_mode:
      self.set_model(Modes.normal_mode, new_episode = False)
    elif contains_fixed_mode:
      # TODO: jos mode == fixed, niin pistä oikeasti to_fixed
      self.set_model(Modes.fixed_mode, new_episode = False)

  @staticmethod
  def set_fixed_joints(actions_dict, joint_static_info):
    mask_actions = {}
    for key in joint_static_info.keys():
      if not joint_static_info[key].edit:
        mask_actions[key] = joint_static_info[key].start

    for action_dict in actions_dict:
      for mask_key in mask_actions.keys():
        action_dict[mask_key] = mask_actions[mask_key]
    
    return actions_dict

  def _apply_mode_constraints(self, actions_dict):
    if self.current_mode == Modes.fixed_mode:
      actions_dict = self.set_fixed_joints(actions_dict, self.info.vertical)

    if self.current_mode == Modes.normal_mode:
      for action_dict in actions_dict:
        # TODO: tarviiko ppo tiedon siitä että liian isot arvo clipataan? Eli ettei vaikka anna arvoksi 100?
        m = self.info.horizontal.front_pitch.max_abs
        action_dict["front_pitch"] = np.clip(action_dict["front_pitch"],-m, m)
        m = self.info.horizontal.back_pitch.max_abs
        action_dict["back_pitch"] = np.clip(action_dict["back_pitch"],-m, m)

    return actions_dict

  def _inputs_to_motor_commands(self, user_inputs, parallel_env_count):
    user_commands = []
    
    for i in range(parallel_env_count):
      if user_inputs is None: 
        user_command = self.info.empty_actions()
      else: 
        user_command = self._user_input_to_motor_commands(user_inputs[i], self.info)

      user_commands.append(user_command)

    return user_commands

  def _prediction_to_commands(self, actoin_np, user_commands, state, allow_agent_actions, info):
    return self.models[self.current_mode].data_processor.prediction_to_commands(actoin_np, user_commands, state, allow_agent_actions, info)
    
  def model_actions_to_motor_commands(self, actions, user_inputs, state):
    actoin_np = actions.cpu().numpy()
    user_commands = self._inputs_to_motor_commands(user_inputs, actoin_np.shape[0])
    actions_dict, descaling_error = self._prediction_to_commands(actoin_np, user_commands, state, self.env_vars.allow_agent_actions, self.info)
    actions_dict = self._apply_mode_constraints(actions_dict)

    return actions_dict, descaling_error

  @staticmethod
  def _get_device():
    has_cuda = torch.cuda.is_available()
    return torch.device("cuda" if has_cuda else "cpu")

  def _step(self):
    def fixed_normal_orchesteration():
      if self.current_mode == Modes.normal_mode:
        if self.mode_counter > self.mode_phaser.value:
          self.set_model(Modes.fixed_mode, new_episode = False)
          self.mode_counter = 0
          return EasyDict({"train_step": False, "reset_episode": False})
        else:
          return EasyDict({"train_step": False, "reset_episode": False})
      else:
        assert self.current_mode == Modes.fixed_mode
        return EasyDict({"train_step": True, "reset_episode": False})

    self.mode_counter += 1
    if self.env_vars.train:
      if self.train_target_mode == Modes.fixed_mode:
        return fixed_normal_orchesteration()
      else:
        raise Exception("pkihug")
        return EasyDict({"train_step": True, "reset_episode": False})
    else:
      return EasyDict({"train_step": False, "reset_episode": False})

  def enrich_action(self, state, env_state, user_action):
    for env_st in env_state:
      env_st["mode"] = self.current_mode

    if self.current_mode == Modes.fixed_mode:
      if self.env_vars.train:
        #action_t = torch.tensor(user_action).to(self.device).to(torch.int64)
        #action_onehot = torch.nn.functional.one_hot(action_t, num_classes = self.vertical_actions).float()
        #state_new = torch.cat((state, action_onehot), 1)
        state_new = PpoDataConverter.add_one_hot_action(user_action, state, self.vertical_actions, self.device)
        for env_st, oh in zip(env_state, user_action):
          env_st["action_request"] = oh

        return state_new, env_state
      else:
        act = 0

        state_new = PpoDataConverter.add_one_hot_action(user_action, state, self.vertical_actions, self.device)
        for env_st in env_state:
          env_st["action_request"] = user_action[0]

        return state_new, env_state

    else:
      # TODO: 
      return state, env_state

  def prediction_model(self, env_state, user_inputs):
    prediction_info = self._step()

    data_processor = self.models[self.current_mode].data_processor
    norm_state = torch.tensor(data_processor.env_state_to_ppo_input(env_state)).float().to(self.device)

    if self.env_vars.train:
      inp_actions = self.user_action_train.get_action()
    elif user_inputs is None:
      inp_actions = [self.test_run_info.last_action]
    else: 
      assert len(user_inputs) == 1
      acts = user_inputs[0]
      is_input_action = lambda x: x == "up" or x == "down"
      acts = list(filter(is_input_action, acts))
      
      if len(acts) == 1:
        inp_actions = 0 if acts[0] == "down" else 1
        self.test_run_info.last_action = inp_actions
        inp_actions = [inp_actions]
      else:
        inp_actions = [self.test_run_info.last_action]

    norm_state, env_state = self.enrich_action(norm_state, env_state, inp_actions)
    #print(self.current_mode,"current_modecurrent_modecurrent_mode" )
    # TODO: Add reqired state
    #luo agenttiin funktio joka ottaa staten ja user commandit. Palauttaa staten johon lisätty user commandit
    model = self.model.eval()
    pred = model(norm_state)
    if prediction_info["train_step"]:
      assert self.current_mode == Modes.fixed_mode
    #print(env_state, "ttttttttttttttttttttttttttt")
    
    #pitää muuttaa palauttamaan normalisoimaton state. Silleen state on aina SI, eikä ppo
    #* lisää "actions"
    #* lisää actions käsittely updateen
    #

    return pred, prediction_info, env_state # EasyDict({"train_step":False, "reset_episode": False})

  def init_model(self, mode):
    lr = 5e-7 if self.learning_rate is None else self.learning_rate
    input_size = self.models[mode].input_size
    output_size = self.models[mode].output_size
    model = ActorCritic(input_size, output_size, mode, std = 0.05).to(self.device)
    optimizer = optim.Adam(model.parameters(), lr=lr, eps=1e-10)

    model, optimizer, start_step = self.file_handler.update_model_params(model, optimizer, mode)
    return model, optimizer, start_step

  def _init_models(self):
    for mode in self.models.keys():
      if self.models[mode]["model"] is None:
        model, optimizer, start_step = self.init_model(mode)
        self.models[mode]["model"] = model
        self.models[mode]["optimizer"] = optimizer
        start_step = 0 if start_step is None else start_step

        self.models[mode]["start_step"] = start_step



  # asettaa modelin vastaamaan tiettyä modea
  # mikäli mallia ei vielä luotu ja alustettu niin hoitaa sen
  # muussa tapauksessa vain asettaa olemassa olevan mallin current malliksi.
  def set_model(self, mode, new_episode):
    self.mode_counter = 0
    self.mode_phaser.reset()
    mode = self._init_current_mode(mode, new_episode)
    model = self.models[mode]["model"]
    optimizer = self.models[mode]["optimizer"]

    self.model = model
    self.optimizer = optimizer

  @property
  def start_step(self):
    return self.models[self.train_target_mode]["start_step"]

  def save_scheduled_model(self, R_avg, start_step):
    return self.file_handler.save_scheduled_model(R_avg, start_step, self.model, self.optimizer, self.current_mode)

  def update(self, episode_info, step):
    self.file_handler.update_best_model(episode_info.rew_avg, step, self.model, self.optimizer, self.current_mode)

    self.model = self.model.train()

    episode_info = PpoDataConverter.to_input(episode_info, self.device, self.info, self.current_mode, self.vertical_actions) 
    self.model, self.optimizer, mean_loss = PpoOperator.ppo_update(self.model, self.optimizer, episode_info, step, self.env_vars)
    self.update_model_state(self.model, self.optimizer, self.current_mode)

    return mean_loss, episode_info.returns

  def config_summary(self):
    summary = f"Best at the begin {str(self.best_reward_begin)}, " +\
              f"Best at the end {str(self.file_handler.best_reward(self.current_mode))}, " +\
              f"Start step {self.start_step}, ppo_epochs {self.ppo_epochs}, mini_batch_size {self.mini_batch_size}"
    return summary

  def update_model_state(self, model, optimizer, mode):
    self.models[mode]["model"] = model
    self.models[mode]["optimizer"] = optimizer

  def _user_input_to_motor_commands(self, keyboard_actions, info):
    if keyboard_actions is None: 
      return
    elif self.current_mode == Modes.normal_mode:  
      return Agent._to_horizontal_motor_commands(keyboard_actions, info)
    elif self.current_mode == Modes.fixed_mode:  
      return Agent._to_vertical_motor_commands(keyboard_actions, info)
    else:
      raise Exception("Keyboard actions not known for mode", self.current_mode)

  def get_reward(self, state_new, state_prev, step_info):
    if self.current_mode == Modes.normal_mode:  
      return self._horizontal_reward(state_new, step_info)
    elif self.current_mode == Modes.fixed_mode:  
      return self._vertical_reward(state_new, state_prev)
    else:
      raise Exception("Reward mode not supported", self.current_mode)

  def motor_force_penalty(self, state):
    limit = 13
    total_force = state.back_right + state.back_left + state.front
    return min(total_force - limit, 0)

  def motor_position_penalty(self, front_motor_z, back_motor_z):
    # Rises vertcally if unit vector of front component is the same as unitvector of gravity., but opposite direction 
    front_penalty = np.absolute(np.array(self.env_vars.gravity_global_direction)+np.array(front_motor_z)).sum()
    back_penalty = np.absolute(np.array(self.env_vars.gravity_global_direction)+np.array(back_motor_z)).sum()

    return -(front_penalty + back_penalty)


  def _get_direction_penalty(self, state, mov_hist):
    def get_angle(arr1, arr2):
      get_x_y = lambda mov: (mov[0]+0.0000001, mov[1]+0.0000001)
      mov_latest = get_x_y(arr1)
      mov_recent = get_x_y(arr2)
      latest_recent_angle = Physics.angle_between(mov_latest, mov_recent)
      return latest_recent_angle

    horizonta_movement_latest = state.horizonta_movement_latest
    direction_hist = state.fuselage2_front[0:2]

    horizonta_movement_prev = mov_hist["position"]["prev"]
    horizonta_direction_prev = mov_hist["direction"]["prev"][0:2]

    latest_recent_angle = get_angle(direction_hist, horizonta_direction_prev)
    horizontal_diff = np.array(horizonta_movement_latest) - np.array(horizonta_movement_prev)
    horizontal_diff = np.absolute(horizontal_diff).sum()

    direction_penalty_movement = horizontal_diff

    return -latest_recent_angle, -direction_penalty_movement

  def _get_movement_penalty(self, state):
    horizontal_diff = np.array(state.horizonta_movement_latest)
    horizontal_diff = np.absolute(horizontal_diff).sum()
    #direction_penalty = direction_penalty_angle + direction_penalty_movement
    return -horizontal_diff
  
  def _update_prev_env(self, index, state):
    self.prev_env[index]["position"]["second_prev"] = self.prev_env[index]["position"]["prev"]
    self.prev_env[index]["position"]["prev"] = state.horizonta_movement_latest
    self.prev_env[index]["direction"]["second_prev"] = self.prev_env[index]["direction"]["prev"]
    self.prev_env[index]["direction"]["prev"] = state.fuselage2_front


  def _horizontal_reward(self, envs_state, step_info):
    # TODO: horizontal_reward tarvitsee tiedon edellisestä actionista
    rewards = []
    for i in range(envs_state.shape[0]):
      state = envs_state[i]
      state = EasyDict(state)

      MAX_SPEED_PENALTY = 15
      speed_penalty = -abs((17-state.speed_fus2_dir) * 1.5)
      #speed_penalty = np.clip(speed_penalty, -MAX_SPEED_PENALTY, 0)
      #print(state.height_delta, "state.height_deltastate.height_delta")
      height_penalty = -abs(state.height_delta - 0.02)*30
      
      tilting_penalty = -1 - state.z_in_local_gravity
      tilting_penalty += -abs(state.x_in_local_gravity)*3
      
      tilting_penalty = tilting_penalty * 10 #50
      assert tilting_penalty <= 0
      #horizontal_movement_prev = self.prev_env[i]

      direction_penalty_angle, direction_penalty_movement = \
        self._get_direction_penalty(state, self.prev_env[i])
      self._update_prev_env(i, state)

      #{'prev': [0, 0], 'second_prev'
      direction_penalty = direction_penalty_angle + direction_penalty_movement*2
      direction_penalty *= 200
      direction_penalty = np.clip(direction_penalty, -100,0)

      #print(height_penalty ,speed_penalty , tilting_penalty, direction_penalty, "ijgijtgitig")
      penalty = height_penalty + speed_penalty + tilting_penalty + direction_penalty
      rewards.append(penalty)

    return rewards

  def _vertical_reward(self, envs_state, envs_state_prev):
    rewards = []    

    for i in range(envs_state.shape[0]):
      state = envs_state[i]
      state = EasyDict(state)

      #motor_force_penalty = self.motor_force_penalty(state) / 10
      
      tilting_penalty = -1 - state.z_in_local_gravity
      tilting_penalty += -abs(state.x_in_local_gravity)
      
      tilting_penalty = tilting_penalty * 15 #50
      assert tilting_penalty <= 0

      #front_motor_vec = Physics.unit_vector(state.front_motor_vec)
      #back_motor_vec = Physics.unit_vector(state.back_motor_vec)
      #motor_position_penalty = self.motor_position_penalty(front_motor_vec, back_motor_vec)
      #motor_position_penalty = motor_position_penalty / 2

      joint_angle_penalty = -abs(state.front_pitch - self.info.vertical.front_pitch.start)*3
      joint_angle_penalty += -abs(state.front_roll -  self.info.vertical.front_roll.start)
      #joint_angle_penalty += -abs(state.back_pitch -  self.info.vertical.back_pitch.start)
      #joint_angle_penalty += -abs(state.back_roll -   self.info.vertical.back_roll.start)
      joint_angle_penalty *= 3

      motor_force_penalty = -abs(state.front - self.info.vertical.front.start)
      motor_force_penalty += -abs(state.back_left - self.info.vertical.back.start)
      motor_force_penalty += -abs(state.back_right - self.info.vertical.back.start)
      motor_force_penalty /= 4

      direction_penalty_angle, direction_penalty_movement = self._get_direction_penalty(state, self.prev_env[i])
      self._update_prev_env(i, state)

      direction_penalty = direction_penalty_angle + direction_penalty_movement*16
      direction_penalty *= 100
      direction_penalty = np.clip(direction_penalty, -20,0)

      if "action_request" not in envs_state_prev[i]:
        print("height_penalty not defined")
        height_penalty = 0
      else:
        input_command = envs_state_prev[i]["action_request"]

        # input_command [0,1]
        input_command = input_command*2 - 1
        
        height_penalty = -abs(input_command - state.height_delta*4)
        height_penalty *= 20
      #height_penalty = -abs(state.height_delta - 0.02)*30

      penalty = motor_force_penalty + tilting_penalty + direction_penalty + joint_angle_penalty + height_penalty
      penalty /= 4

      assert penalty < 0
      #print(motor_force_penalty, tilting_penalty,direction_penalty, joint_angle_penalty,height_penalty, "tyhtyth", penalty)
      rewards.append(penalty)
    
    return rewards

  @staticmethod
  def _to_horizontal_motor_commands(keystrokes, info):
    actions = info.empty_actions()
    pitch_amplifier = 0.03
    roll_amplifier = 0.03
    motor_amplifier = 2
    back_motor_amplifier = 1.5
    for keystroke in keystrokes:
      if keystroke == "w":
        actions.front_pitch += pitch_amplifier
        actions.back_pitch += pitch_amplifier
      elif keystroke == "s":
        actions.front_pitch -= pitch_amplifier
        actions.back_pitch -= pitch_amplifier
      elif keystroke == "a":
        actions.front_roll -= roll_amplifier
        actions.back_roll -= roll_amplifier
      elif keystroke == "d":
        actions.front_roll += roll_amplifier
        actions.back_roll += roll_amplifier        
      elif keystroke == "e":
        actions.front_roll -= roll_amplifier
        actions.back_roll += roll_amplifier
      elif keystroke == "q":
        actions.front_roll += roll_amplifier
        actions.back_roll -= roll_amplifier
      elif keystroke == "up":
        actions.front += motor_amplifier
        actions.back_left += back_motor_amplifier*motor_amplifier
        actions.back_right += back_motor_amplifier*motor_amplifier
      elif keystroke == "down":
        actions.front -= motor_amplifier
        actions.back_left -= back_motor_amplifier*motor_amplifier
        actions.back_right -= back_motor_amplifier*motor_amplifier

    return actions

  @staticmethod
  def _to_vertical_motor_commands(keystrokes, info):
    actions = info.empty_actions()
    pitch_amplifier = 0.03
    roll_amplifier = 0.03
    
    motor_amplifier = 3
    back_motor_amplifier = 0.7

    for keystroke in keystrokes:
      if keystroke == "w":
        actions.front_pitch += pitch_amplifier
        actions.back_pitch += pitch_amplifier
      elif keystroke == "s":
        actions.front_pitch -= pitch_amplifier
        actions.back_pitch -= pitch_amplifier
      elif keystroke == "a":
        actions.front_roll -= roll_amplifier
        actions.back_roll -= roll_amplifier
      elif keystroke == "d":
        actions.front_roll += roll_amplifier
        actions.back_roll += roll_amplifier
      elif keystroke == "e":
        actions.front_roll -= roll_amplifier
        actions.back_roll += roll_amplifier
      elif keystroke == "q":
        actions.front_roll += roll_amplifier
        actions.back_roll -= roll_amplifier
      #elif keystroke == "up":
      #  actions.front += 1*motor_amplifier
      #  actions.back_left += back_motor_amplifier*motor_amplifier
      #  actions.back_right += back_motor_amplifier*motor_amplifier
      #elif keystroke == "down":
      #  actions.front -= 1*motor_amplifier
      #  actions.back_left -= back_motor_amplifier*motor_amplifier
      #  actions.back_right -= back_motor_amplifier*motor_amplifier

    return actions
