from easydict import EasyDict
import numpy as np
import math
import torch
import copy
from models.ppo.ppo import PpoOperator
from models.modes import Modes

class EnvToPpoNorm:
  def __init__(self, info, mode):
    self.info = info
    self.mode = mode
  
  @staticmethod
  def scaled_val(val, limits):
    return ((val - limits.lower) / (limits.upper - limits.lower) - 0.5) * 2
  @staticmethod
  def normalized_spin(dz):
    SPIN_NORMALIZER = math.pi/20
    return dz/SPIN_NORMALIZER
  
  def front_pitch(self, val):
    return self.scaled_val(val, self.info.joint_info.front_pitch)
  def front_roll(self, val):
    return self.scaled_val(val, self.info.joint_info.front_roll)
  def back_pitch(self, val):
    return self.scaled_val(val, self.info.joint_info.back_pitch)
  def back_roll(self, val):
    return self.scaled_val(val, self.info.joint_info.back_roll)
  def front(self, val):
    return self.scaled_val(val, self.info.motor_force.front)
  def back_left(self, val):
    return self.scaled_val(val, self.info.motor_force.back)
  def back_right(self, val):
    return self.scaled_val(val, self.info.motor_force.back)
  def speed_fus2_dir(self, val):
    return val / 10
  def gravity_angle_xz(self, val):
    return val / math.pi
  def gravity_angle_yz(self, val):
    return val / math.pi
  def recent_gravity_angle_xz_avg(self, val):
    return val / math.pi
  def recent_gravity_angle_yz_avg(self, val):
    return val / math.pi
  def x_in_local_gravity(self, val):
    return val
  def y_in_local_gravity(self, val):
    return val
  def z_in_local_gravity(self, val):
    return val
  def height(self, val):
    val = (val/2)-1
    return np.clip(val,-1,1)
  def height_delta(self, val):
    return np.clip(val*5, -1,1)
  def speed(self, val):
    return val/10 - 1


  def get_norm_func(self, name):
    return getattr(self, name)

class PpoDataConverter:
  act_index = EasyDict({"front":0, "back_left":1, "back_right":2, "front_pitch":3, "front_roll":4, "back_pitch":5, "back_roll":6})

  def __init__(self, info, mode):
    self.mode = mode
    self.info = info
    input_field_names_fixed = ["gravity_angle_xz", "gravity_angle_yz", "recent_gravity_angle_xz_avg", "recent_gravity_angle_yz_avg",
    "speed_fus2_dir","x_in_local_gravity","y_in_local_gravity","z_in_local_gravity","front", "back_left", 
    "back_right", "front_pitch", "front_roll", "back_pitch", "back_roll", "height_delta", "speed"]
    
    state_info_fixed = {}
    for i, name in enumerate(input_field_names_fixed):
      state_info_fixed[name] = {"index":i, "name":name}
    
    input_field_names_normal = input_field_names_fixed
    state_info_normal = {}
    for i, name in enumerate(input_field_names_normal):
      state_info_normal[name] = {"index":i, "name":name}

    self.field_info = EasyDict({Modes.fixed_mode: state_info_fixed, 
                                Modes.normal_mode: state_info_normal})


  # TODO: Ei ole enää one hot
  @staticmethod
  def add_one_hot_action(action, target, actoin_cnt, device):
      action_t = torch.tensor(action).to(device).to(torch.int64)
      action_onehot = torch.nn.functional.one_hot(action_t, num_classes = actoin_cnt).float()
      action_onehot = action_onehot*2 - 1

      return torch.cat((target, action_onehot), 1)

  @staticmethod
  def to_input(episode_info, device, info, mode, actoin_cnt):
    states = episode_info.states 
    actions = episode_info.actions 
    log_probs = episode_info.log_probs 
    rewards = episode_info.rewards 
    values = episode_info.values 
    masks = episode_info.masks
    action_dist = episode_info.action_distribution
    
    masks = [torch.FloatTensor(mask).unsqueeze(1).to(device) for mask in masks]
    rewards, std = PpoDataConverter.scale_reward(rewards)
    rewards = [ torch.FloatTensor(r).unsqueeze(1).to(device) for r in rewards]

    # next_value is last value of data gathering runs. It is only used to calculate gae. It was not used to take actions.
    next_value = values[-1]
    values = values[:-1]
    returns = PpoOperator.compute_gae(next_value, rewards, masks, values)
    
    #sts = np.array(states)
    #print()
    #print(states[0], "igjtijgjtgjtkkkkkkkkkkkkkkkk")
    #print(states[0])
    
    flatten = lambda t: [item for sublist in t for item in sublist]
    #states = flat_list = [item for sublist in t for item in sublist]
    states = flatten(states)
    
    #states = sts.reshape((-1))

    action_requests = [a["action_request"] for a in states]
    
    # TODO: Clean ppo converter usage
    #print(type(states[0]),"statesstatesstatesstates", states[0] )
        # ['rew_avg', 'reward_std', 'log_probs', 'values', 'states', 'actions', 'masks', 'entropy', 'rewards', 'action_dist']
    #print(list(episode_info.keys()))

    #print(episode_info.states[0], "ghtgtgtghutghutg")
    states = PpoDataConverter(info, mode).env_state_to_ppo_input(states)

    returns   = torch.cat(returns).detach().to(device).float()
    log_probs = torch.cat(log_probs).detach().to(device).float()
    values    = torch.cat(values).detach().to(device).float()
    states    = torch.tensor(states).detach().to(device).float()


    states = PpoDataConverter.add_one_hot_action(action_requests, states, actoin_cnt, device)
    #norm_state, env_state = self.enrich_action(norm_state, env_state, action_requests)

    actions   = torch.cat(actions).detach().to(device).float()

    advantage = returns - values

    action_dist.loc = torch.cat(action_dist.loc).detach().to(device).float()
    action_dist.scale = torch.cat(action_dist.scale).detach().to(device).float()

    episode_info.states = states 
    episode_info.actions = actions 
    episode_info.log_probs = log_probs 
    episode_info.returns = returns 
    episode_info.advantage = advantage 
    episode_info.action_dist = action_dist
    
    return episode_info

  def _convert_env_to_ppo(self, env_state_batch):
    mode = self.mode
    ppo_state_batch = []
    ppo_input_size = len(self.field_info[mode])

    for env_state in env_state_batch:
      ppo_state = np.empty(ppo_input_size)
      ppo_state[:] = np.nan

      for input_field in self.field_info[mode]:
        input_index = self.field_info[mode][input_field].index
        ppo_state[input_index] = env_state[input_field]

      assert np.isnan(ppo_state).sum() == 0
      ppo_state_batch.append(ppo_state)

    return ppo_state_batch


  def env_state_to_ppo_input(self, env_state_batch):
    def normalize_ppo_input(field_info, normalizer):
      normalization_function_dict_fixed = {}
      for key in field_info.keys():
        normalization_function_dict_fixed[key] = normalizer.get_norm_func(key)

      def norm_inp(env_state_dict):
        ppo_input_fields = {}
        input_field_names = list(field_info)
        for key in input_field_names:
          #print(key, "ytytytytytyty", env_state_batch.shape)
          #print(env_state_dict, "env_state_dictenv_state_dict")
          #raise Exception("errttyy")
          ppo_input_fields[key] = normalization_function_dict_fixed[key](env_state_dict[key])

        return ppo_input_fields

      return norm_inp
    
    norm_func = normalize_ppo_input(self.field_info[self.mode], EnvToPpoNorm(self.info, self.mode))
    normalized = list(map(norm_func, env_state_batch))
    ppo_state_batch = self._convert_env_to_ppo(normalized)
    return np.array(ppo_state_batch).astype(float)

  @staticmethod
  def scale_reward(rewards):
    rewards = np.array(rewards)
    std = rewards.flatten().std()

    return (rewards / std).tolist(), std

  @staticmethod
  def action_to_motor_commands(action, info):
    def model_output_to_unit_scale(val):
      model_out_max = 1
      
      model_out_min = -1
      return (val - model_out_min) / (model_out_max - model_out_min)

    def descale_model_output(val, limits, descaling_error):
      normalized_to_one = model_output_to_unit_scale(val)
      descaled = normalized_to_one * (limits.upper - limits.lower) + limits.lower
      chopped = np.clip(descaled, limits.lower, limits.upper)
      # Decaling error is only for model prediction
      descaling_error += abs(chopped - descaled) / (limits.upper - limits.lower)
     
      return chopped, descaling_error

    descaling_error = 0
    mf = info.motor_force
    front_motor, descaling_error = descale_model_output(action["front"], mf.front, descaling_error)
    back_left_motor, descaling_error = descale_model_output(action["back_left"], mf.back, descaling_error)
    back_right_motor, descaling_error = descale_model_output(action["back_right"], mf.back, descaling_error)
    
    ji = info.joint_info
    # TODO: laske miten  front_pitch mean muuttuu
    front_pitch, descaling_error = descale_model_output(action["front_pitch"], ji.front_pitch, descaling_error)
    front_roll, descaling_error = descale_model_output(action["front_roll"], ji.front_roll, descaling_error)
    back_pitch, descaling_error = descale_model_output(action["back_pitch"], ji.back_pitch, descaling_error)
    back_roll, descaling_error = descale_model_output(action["back_roll"], ji.back_roll, descaling_error)
    
    return {"front": front_motor,
            "back_left": back_left_motor,
            "back_right": back_right_motor,
            "front_pitch": front_pitch,
            "front_roll": front_roll,
            "back_pitch": back_pitch,
            "back_roll": back_roll}, descaling_error


  @staticmethod
  def add_keyboard_actions(action, keyboard_additions, info):
    def add_keyboard_act(val, limits, keyboard_addition):
      return np.clip(val + keyboard_addition, limits.lower,limits.upper)
      
    keyboard_additions = info.empty_actions() if keyboard_additions is None else keyboard_additions

    mf = info.motor_force

    front_motor = add_keyboard_act(action["front"], mf.front, keyboard_additions.front)
    back_left_motor = add_keyboard_act(action["back_left"], mf.back, keyboard_additions.back_left)
    back_right_motor = add_keyboard_act(action["back_right"], mf.back, keyboard_additions.back_right)
    
    ji = info.joint_info
    front_pitch = add_keyboard_act(action["front_pitch"], ji.front_pitch, keyboard_additions.front_pitch)
    front_roll = add_keyboard_act(action["front_roll"], ji.front_roll, keyboard_additions.front_roll)
    back_pitch = add_keyboard_act(action["back_pitch"], ji.back_pitch, keyboard_additions.back_pitch)
    back_roll = add_keyboard_act(action["back_roll"], ji.back_roll, keyboard_additions.back_roll)
    
    return {"front": front_motor,
            "back_left": back_left_motor,
            "back_right": back_right_motor,
            "front_pitch": front_pitch,
            "front_roll": front_roll,
            "back_pitch": back_pitch,
            "back_roll": back_roll}

  def prediction_to_commands(self, actions, user_commands, old_motor_state, allow_agent_actions, info):
    def current_state_as_commands(state):
      motor_state = {}
      fi = self.field_info[self.mode]

      motor_state["front_pitch"] = state[fi.front_pitch.name]
      motor_state["front_roll"] = state[fi.front_roll.name]
      motor_state["back_pitch"] = state[fi.back_pitch.name]
      motor_state["back_roll"] = state[fi.back_roll.name]    
      motor_state["front"] = state[fi.front.name]
      motor_state["back_left"] = state[fi.back_left.name]
      motor_state["back_right"] = state[fi.back_right.name]

      return EasyDict(motor_state)

    commands = []
    a = PpoDataConverter.act_index
    descaling_errors = []

    for i in range(actions.shape[0]):
      action = actions[i]
      action = {"front": action[a.front],
              "back_left": action[a.back_left],
              "back_right": action[a.back_right],
              "front_pitch": action[a.front_pitch],
              "front_roll": action[a.front_roll],
              "back_pitch": action[a.back_pitch],
              "back_roll": action[a.back_roll]}

      if allow_agent_actions:
        command, descaling_error = PpoDataConverter.action_to_motor_commands(action, info)
        target = PpoDataConverter.add_keyboard_actions(command, user_commands[i], info)
      else:
        descaling_error = 0
        target = PpoDataConverter.add_keyboard_actions(current_state_as_commands(old_motor_state[i]), user_commands[i], info)

      commands.append(target)
      descaling_errors.append(descaling_error)
    return commands, np.array(descaling_errors)
