# https://github.com/higgsfield/RL-Adventure-2
#import random
from easydict import EasyDict

import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.distributions import Normal
from scipy.stats import norm
from models.modes import Modes



def init_weights(m):
    if isinstance(m, nn.Linear):
        nn.init.normal_(m.weight, mean=0., std=0.01)
        nn.init.constant_(m.bias, 0.01)

def activation_function():
  #return nn.Tanh()
  return nn.ReLU()

"""
class ResidualBlock(nn.Module):
  def __init__(self, hidden_size):
    super().__init__()
    self.activate = activation_function()
    self.shortcut = nn.Identity()   

    self.blocks = nn.Sequential(
      nn.Linear(hidden_size, hidden_size),
      nn.BatchNorm1d(num_features=hidden_size),
      activation_function(),
      nn.Linear(hidden_size, hidden_size),
      nn.BatchNorm1d(num_features=hidden_size)
    )

  def forward(self, x):
    residual = self.shortcut(x)
    x = self.blocks(x)
    x += residual
    x = self.activate(x)
    return x
"""
        
class ActorCritic(nn.Module):
  def __init__(self, num_inputs, num_outputs, mode, std=0.0):
      super(ActorCritic, self).__init__()
      if mode == Modes.fixed_mode:
        hidden_size = 512 #128
      else: 
        hidden_size = 256 #128

      common_size2 = int(hidden_size)

      #https://pytorch.org/docs/stable/nn.html#normalization-layers
      self.critic = nn.Sequential(
        nn.BatchNorm1d(num_features=num_inputs),
        nn.Linear(num_inputs, common_size2),
        activation_function(),
        #ResidualBlock(common_size2),
        nn.Linear(common_size2, common_size2), #
        activation_function(),# -r
        nn.Linear(common_size2, hidden_size), # -r
        activation_function(), # -r
        nn.Linear(hidden_size, 1) #-r
        #nn.Linear(common_size2, 1) #r
      )
      
      self.actor = nn.Sequential(
        nn.BatchNorm1d(num_features=num_inputs),
        nn.Linear(num_inputs, hidden_size),
        activation_function(),
        #ResidualBlock(hidden_size), # r
        nn.Linear(hidden_size, hidden_size),# -r
        activation_function(), # -r
        nn.Linear(hidden_size, num_outputs),
        #nn.Sigmoid()
        nn.Tanh()
      )
      
      # Deep RL Hands On p507 käyttää linear + softplus
      self.log_std = nn.Parameter(torch.ones(1, num_outputs) * std)
      self.apply(init_weights)
      
  def forward(self, x):
    value = self.critic(x)
    mu    = self.actor(x)
    std   = self.log_std.exp().expand_as(mu)
    dist  = Normal(mu, std)
    #print(mu, "igjtijgtij")
    # action indeksit "front_pitch":3
    return dist, value

class PpoOperator:
  @staticmethod
  def compute_gae(next_value, rewards, masks, values, gamma=0.98, tau=0.98):
    values = values + [next_value]
    gae = 0
    returns = []
    for step in reversed(range(len(rewards))):
      delta = rewards[step] + gamma * values[step + 1] * masks[step] - values[step]
      gae = delta + gamma * tau * masks[step] * gae
      returns.insert(0, gae + values[step])

    return returns

  @staticmethod
  def ppo_iter(mini_batch_size, states, actions, log_probs, returns, advantage, actions_dist):
    batch_size = states.size(0)
    for _ in range(batch_size // mini_batch_size):
      rand_ids = np.random.randint(0, batch_size, mini_batch_size)
      
      yield states[rand_ids, :], actions[rand_ids, :], log_probs[rand_ids, :], returns[rand_ids, :], advantage[rand_ids, :], actions_dist.loc[rand_ids], actions_dist.scale[rand_ids]

  @staticmethod
  def calc_KL(old_loc, old_scale, new_loc, new_scale):
    def actions_kl(old_loc, old_scale, new_loc, new_scale, kl_div):
      # 7 actions so zippet param size is 7
      #kl_div = 0
      for ol, os, nl, ns in zip(old_loc, old_scale, new_loc, new_scale):
        p = norm.pdf(kl_points, ol,os)
        q = norm.pdf(kl_points,nl,ns)

        kl_div.append(kl_divergence(q, p))
      return kl_div

    def kl_divergence(p, q):
      return np.sum(np.where(p != 0, p * np.log(p / q), 0))

    kl_points = np.arange(-3, 3, 0.1)

    to_np = lambda x: x.cpu().detach().numpy() 
    # params contains batch_size different locs and scales
    kl_div = []
    for ol, os, nl, ns in zip(to_np(old_loc), to_np(old_scale), to_np(new_loc), to_np(new_scale)):
      kl_div = actions_kl(ol, os, nl, ns, kl_div)

    kl_div = np.array(kl_div)
    return kl_div.mean(), kl_div.max() #/(old_loc.shape[0]*old_loc.shape[1])

  @staticmethod
  def log_gradients(model, writer, step, grad_count, grad_max, grad_means):
    for p in model.parameters():
      grad_max = max(grad_max, p.grad.abs().max().item())
      grad_means += (p.grad ** 2).mean().sqrt().item()
      grad_count += 1 

    return grad_count, grad_max, grad_means

  @staticmethod
  def ppo_update(model, optimizer, episode_info, step, env_vars):
    states = episode_info.states 
    actions = episode_info.actions 
    log_probs = episode_info.log_probs 
    returns = episode_info.returns 
    advantages = episode_info.advantage
    actions_dist = episode_info.action_distribution    

    clip_param=0.1
    losses = []
    ppo_epochs = env_vars.ppo_epochs
    mini_batch_size = env_vars.mini_batch_size

    kl_mean = []
    kl_max = []
    grad_max = 0.0
    grad_means = 0.0
    grad_count = 0
    CLIP_GRAD = 0.05

    for ind in range(ppo_epochs):
      for state, action, old_log_probs, return_, advantage, loc, scale in PpoOperator.ppo_iter(mini_batch_size, states, actions, log_probs, returns, advantages, actions_dist):
        ps = []
        
        dist, value = model(state)

        #new_log_probs = dist.log_prob(action)

        # Entropy luokkaa 2.6. Jos ei alusteta niin luokkaa 1.6
        entropy = dist.entropy().mean()
        new_log_probs = dist.log_prob(action)

        # on sama kuin policy_new(S)/policy_old(S). Eli on suunilleen 1. 
        # * Jostain syystä ei ole ensimmäiselläkään kierroksella tasna 1.
        # * Joten model joka loi vanhan log_prbsin oli eri kuin joka loi uuden.
        # * taitaa liittyä jotenkin siihen, että old on eval() modessa ja new on train() modessa.
        ratio = (new_log_probs - old_log_probs).exp()
        surr1 = ratio * advantage
        # torch.nn.utils.clip_gradn_norm
        surr2 = torch.clamp(ratio, 1.0 - clip_param, 1.0 + clip_param) * advantage

        # onko steps size implisiittisesti
        actor_loss  = - torch.min(surr1, surr2).mean()
        critic_loss = (return_ - value).pow(2).mean()

        # NOTE: muutin entropy kertoimne arvosta 0.001. loss oli luokkaa 0.05
        # arvolla 1.0 loss luokkaa 2.8
        # This is optimized for shared parameters? https://arxiv.org/pdf/2002.07717.pdf
        loss = 0.5 * critic_loss + actor_loss - 0.001 * entropy

        optimizer.zero_grad()
        loss.backward()

        nn.utils.clip_grad_norm_(model.parameters(), CLIP_GRAD)
        
        optimizer.step()
        losses.append(loss.cpu().data.numpy())

        if ind + 1 == ppo_epochs:
          if env_vars.debug.KL:
            k, m = PpoOperator.calc_KL(loc, scale, dist.loc, dist.scale)
            kl_mean.append(k)
            kl_max.append(m)
          
          if env_vars.debug.grad:
            grad_count, grad_max, grad_means = PpoOperator.log_gradients(model, env_vars.writer, step, grad_count, grad_max, grad_means)
            

    
    mean_loss = np.array(losses).mean()
    if env_vars.debug.KL:
      env_vars.writer.add_scalar('kl_mean', np.array(kl_mean).mean(), step)
      env_vars.writer.add_scalar('kl_max', np.array(kl_max).max(), step)
    
    if env_vars.debug.grad:
      env_vars.writer.add_scalar('grad_l2', grad_means / grad_count, step)
      env_vars.writer.add_scalar('grad_max', grad_max, step)


    return model, optimizer, mean_loss
      


    