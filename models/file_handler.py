import torch
from time import gmtime, strftime
from easydict import EasyDict
from pathlib import Path
from models.modes import Modes
import numpy as np
import copy

class AgentFileHandler:

  def __init__(self, env_vars):
    self.env_vars = env_vars

    self.scheduled_subfolder = "scheduled"
    self.best_subfolder = "best"
    self.mode_info = self._set_mode_info(env_vars.mode_info)

  @staticmethod
  def _set_mode_info(mode_info):
    mode_info = copy.deepcopy(mode_info)
    mode_info[Modes.fixed_mode]["folder"] = "fixed"
    mode_info[Modes.normal_mode]["folder"] = "normal"
    mode_info = EasyDict(mode_info)

    return mode_info


  @staticmethod
  def load_model(file_name, model, optimizer, lr = None, load_old_params = True):
    def update_lr(lr, optimizer):
      for param_group in optimizer.param_groups:
        param_group['lr'] = lr

      return optimizer

    start_step = None

    if load_old_params:
      checkpoint = torch.load("model_params/"+file_name)
      model.load_state_dict(checkpoint['model'])
      optimizer.load_state_dict(checkpoint['optimizer'])
      start_step = checkpoint['start_step']
      
      print("setting model from", "model_params/"+file_name)
      if lr is not None:
        optimizer = update_lr(lr, optimizer)

    return optimizer, model, start_step

  def update_best_model(self, reward, step, model, optimizer, mode):
    if reward > self.best_reward(mode):
      self._delete_old_best_checkpoint(mode)
      self._save_current_model_as_best(reward, step, model, optimizer, mode)
    
  def _save_current_model_as_best(self, R_avg, start_step, model, optimizer, mode):
    return self.save_model(model, optimizer, R_avg, self.env_vars.version, start_step, self.best_subfolder, False, mode)

  def save_scheduled_model(self, R_avg, start_step, model, optimizer, mode):
    return self.save_model(model, optimizer, R_avg, self.env_vars.version, start_step, self.scheduled_subfolder, True, mode)

  def _get_params_file_name(self, subfolder, mode, run_id, run_number=None):
    identifier = self._params_file_identifier(run_id, run_number)
    folder = self.mode_info[mode].folder
    p = Path(f'./model_params/{folder}/{subfolder}/')
    return list(p.glob('*' + identifier + '*'))

  def _delete_old_best_checkpoint(self, mode):
    #folder = self.models[self.current_mode].folder
    #p = Path(f'./model_params/{folder}/best/')
    #file_to_del = list(p.glob('*' + self.env_vars.run_id + '*'))
    file_to_del = self._get_params_file_name(self.best_subfolder, mode, self.env_vars.run_id)
    if len(file_to_del) == 0:
      return
    elif len(file_to_del) == 1:
      file_to_del[0].unlink()
    else:
      raise Exception("Too many best files found with run id", self.env_vars.run_id)

  def best_reward(self, mode):
    p = Path('./model_params/' + self.mode_info[mode].folder + "/" + self.best_subfolder)
    file_to_del = list(p.glob('*' + self.env_vars.run_id + '*'))

    # no earlier best reward for this run. Start with way too small reward.
    if file_to_del == []:
      return -1000

    name_components = file_to_del[0].name.split("_")
    assert len(name_components) == 4
    best_reward = name_components[2]

    return float(best_reward)

  def update_model_params(self, model, optimizer, mode):
    given_file_name = self.mode_info[mode].file

    #self._params_file_identifier(run_id, run_number)
    run_file = self._get_params_file_name(self.scheduled_subfolder, mode, self.env_vars.run_id, self.env_vars.run_number - 1)
    learning_rate = self.env_vars.learning_rate
    start_step = None
    if run_file != []:
      assert len(run_file) == 1
      file_name = "/".join(run_file[0].parts[1:])
      optimizer, model, start_step = self.load_model(file_name, model, optimizer, lr=learning_rate)
    elif given_file_name is not None:
      file_name = self.mode_info[mode].folder + "/" + given_file_name
      optimizer, model, start_step = self.load_model(file_name, model, optimizer, lr=learning_rate)
    else: 
      print("No parameter uploading")
    return model, optimizer, start_step

  @staticmethod
  def _params_file_identifier(run_id, run_number):
    if run_number is not None:
      return run_id + "_" +  str(run_number)
    else:
      return run_id
    
  def save_model(self, model, optimizer, R_avg, version, start_step, sub_folder, with_run_nr, mode):
    R_avg_str = str(np.round(R_avg,3))
    run_number = self.env_vars.run_number if with_run_nr else None
    name_postfix = self._params_file_identifier(self.env_vars.run_id, run_number)
    #name_postfix = self.env_vars.run_id + "_" +  str(self.env_vars.run_number)
    time_str = strftime("%Y%m%d%H%M%S", gmtime())
    path = self.mode_info[mode].folder
    file_name = f"model_params/{path}/{sub_folder}/{version}_{time_str}_{R_avg_str}_{name_postfix}"
    torch.save({
        'model': model.state_dict(),
        'optimizer': optimizer.state_dict(),
        'start_step': start_step
        }, file_name)

    return file_name
