import numpy as np
import math


class Physics:

  @staticmethod
  def drag_area(speed_vec, dimensions, comp_info):
    # TODO: Make sure this way of calculating drag area of box is correct
    x_c, y_c, z_c = dimensions
    x,y,z= Physics.unit_vector(speed_vec)
    area_y = y*x_c*z_c 
    if comp_info:
      # Those wing segments that are in the middle of wing wont have drag area in x-dimension.
      # TODO: Verify that - piece code is correct with -x
      if comp_info["piece_code"] == 0 or np.sign(comp_info["piece_code"]) == np.sign(x):
        area_x = 0
      else:
        area_x = x*y_c*z_c
      
    else:
      area_x = x*y_c*z_c 
    area_z = z*y_c*x_c
    return np.absolute([area_x, area_y, area_z])
    

  @staticmethod
  def drag_coeff_body(unit_vel_of_dim):
    """
    If velocity is directly towards dimension, then drag coeff is max 1.2. 
    Otherwise drag coeff is less than max.

    This is very rough quess how coeff could work.

    https://wright.nasa.gov/airplane/shaped.html
    """

    return 1.2 - (1 - np.absolute(unit_vel_of_dim))

  
  #@staticmethod
  #def fuselag_drag_coeff():
  #  # TODO: Actually depends on angle between dimensions. If velocity directly agains one side, then flat surface coeff. Of 45% to two dimensions, then much lower coeff.
  #  # Drag area appromximation mitigates this approximation issue.
  #  #https://wright.nasa.gov/airplane/shaped.html
  #  return 1.2

  @staticmethod
  def drag_coeff_lift(angle_of_attach, lift_coeff, dimensions):
    # if angle_of_attach == 0, then min coeff, which is 0.045. if angle_of_attach == Pi then max coeff, which is 1.2. Otherwise linear
    # https://wright.nasa.gov/airplane/drageq.html
    span, chord, thickness = dimensions
    aspect_ratio = span / chord
    coeff0 = abs(math.sin(angle_of_attach)) * (1.2-0.045) #+ 0.09 # 0.09 because fuselag adds some drag
    
    drag_lift = lift_coeff**2/(math.pi*aspect_ratio*0.7)
    return coeff0 + drag_lift
    
  # TODO: There must be some friction in surface. So drag force is not purely components of velocity direction push.
  # * Notice that friction force does not go through centerpoint. So it will rotate object
  # If friction is added the same amount of energy needs to be removed from other drag
  @staticmethod
  def get_drag(coeffs, speed_vec_local, drag_areas):
    def calc_drag(speed, area, coeff):
      abs_froce = 0.5*coeff*1.2*speed**2*area
      # np.sign is to add direction. 
      return np.sign(speed)*abs_froce
      #return abs_froce

    area_x, area_y, area_z = drag_areas
    # minus speed_vec_local because drag force points opposite direction than velocity
    speed_x, speed_y, speed_z = -speed_vec_local
    force_x = calc_drag(speed_x, area_x, coeffs[0])
    force_y = calc_drag(speed_y, area_y, coeffs[1])
    force_z = calc_drag(speed_z, area_z, coeffs[2])
    #print(speed_vec_local[1], force_y, "force_yforce_yforce_y", speed_y, area_y, coeffs[1])
    return [force_x,force_y,force_z]

  #@staticmethod
  #def get_drag(coeff, speed, area):
  #  return 0.5*coeff*1.2*speed**2*area
 
  @staticmethod
  def unit_vector(vector):
    """ Returns the unit vector of the vector.  """
    if np.absolute(vector).sum() < 1e-10:
      print("Warning unit vector taken from zero vec. Small epsilon added to avoid exceptions")
      return vector / np.linalg.norm(np.array(vector) + 0.000001)
    return vector / np.linalg.norm(vector)

  #@staticmethod
  #def angle_between2(v1, v2):
  #  v1_u = Physics.unit_vector(v1)
  #  v2_u = Physics.unit_vector(v2)
  #  dot_prod = np.clip(np.dot(v1_u, v2_u), -1.0, 1.0)
  #  return np.arcsin(dot_prod)
    
  @staticmethod
  def angle_between(v1, v2):
    v1_u = Physics.unit_vector(v1)
    v2_u = Physics.unit_vector(v2)
    return np.arccos(np.clip(np.dot(v1_u, v2_u), -1.0, 1.0))

  @staticmethod
  def lift_coeff(angle_of_attach, speed_vec_local_unit):
    def get_coeff():   
      angle_of_attach_deg = 57.3 * angle_of_attach
      # https://upload.wikimedia.org/wikipedia/commons/d/d1/Lift_curve.svg
      if angle_of_attach_deg < -5:
        return 0
      elif angle_of_attach_deg < 10:
        return (angle_of_attach_deg + 5) / 10
      elif angle_of_attach_deg < 15:
        return 1.5 + (angle_of_attach_deg-10)/25
      elif angle_of_attach_deg < 19:
        return 1.7
      elif angle_of_attach_deg < 25:
        return 1.7-(angle_of_attach_deg-19)/20
      elif angle_of_attach_deg < 30:
        return 1.4-(angle_of_attach_deg-25)/4
      else:
        return 0

    x_vel_local_coord = abs(speed_vec_local_unit[0])
    if x_vel_local_coord > 0.45:
      return 0

    coeff = get_coeff()
    coeff = coeff*(1-2*x_vel_local_coord)
    return coeff


  @staticmethod
  def get_lift(area, speed, coeff):
    # actually is a function of temperature
    p = 1.2041
    return 1/2*coeff*p*speed**2*area