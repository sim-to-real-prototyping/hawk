import time
import math
import numpy as np
from datetime import datetime
import argparse
from easydict import EasyDict
import random
from pathlib import Path
from torch.utils.tensorboard import SummaryWriter
from time import gmtime, strftime

from multiprocessing_env import SubprocVecEnv
from models.modes import Modes
#from keyboards.system_control import Modes
from env import Env
from simulator import Simulator
from vehicle.hawk_info import HawkInfo
from models.agent import Agent
import os

def str2bool(v):
  if isinstance(v, bool):
    return v
  if v.lower() in ('yes', 'true', 't', 'y', '1'):
    return True
  elif v.lower() in ('no', 'false', 'f', 'n', '0'):
    return False
  else:
    raise argparse.ArgumentTypeError('Boolean value expected.')

parser = argparse.ArgumentParser(description='Simulate RC-plane')
parser.add_argument('-t','--terminal', type=str, required=True)
parser.add_argument('-u','--run-id', type=str, required=True, help="Uuid that is unique for each separate training run")
parser.add_argument('-d','--headless', type=str2bool, default=True)
parser.add_argument('-e','--num-envs', type=int, default=1)
parser.add_argument('-r','--run-number', type=int, default=0)
#parser.add_argument('-f','--params-file-name', type=str, default=None)
#parser.add_argument('-l','--learning-rate', type=float, default=None)

args = parser.parse_args()
#headless = args.headless


# TODO: Deep RL hands on p507 clippaa actionit [-1,1] voisi kokeilla
"""
miten penalty/reward:
* yritä aina oikaista oikeaan asentoon
** anna penaltia jos fuselag below on kaukana z = -1. Tämän pitäisi ratkaista roll ja pitch korjaus
* jos koskee maata niin iso penalty
* ohjauspenalty:
** ota fuselag2 front ennen ohjausta. 
** laske ohjaussignaalin perusteella tavoitetila
** anna mallin ottaa action.
** laske penalty tavoitetilan ja oikean tilan erotuksena
"""

#TODO: Seuraavat stepit
"""

LUE: https://arxiv.org/abs/1804.10332


taskit: 

laske penaltya sen mukaan paljon kokonaiskiihtyvyys eroaa gravitaation suunnasta. [0,0,-1]. 
"""

# TODO: Tallenna learning rate ja run_nr parameters fileen.


# TODO:
# TODO:
# ENSIN
#jos painan "u", niin current_mode = normal, jos "i", niin curent mode on fixed

# LOPUKSI
#list_keyboard_actions jos headless, niin pitää tietää genroidaanko actionit
#* miten erottaa eri moodien actionit?
#** agentin pitää tietää. 
#*** rewardin laskennnan pitää sada tietää annettu komento. Alussa ainoa action on height diff. Rew päättää mikä on tavoitetila.
#**** varmistä että height diff penalty lasketaan vasta kun annetun actionin simulation step on otettu eikä silloin kun action on päätetty
#*** agentti päättää että seuraavaksi noustaan ylöspäin. antaa one hot inputin [alas, vakio, ylös]. Päätös tehdään aina N stepiksi kerrallaan
#Muuta keyboard actionit. Tällä hetkellä actionit menee suoraan moottorieille. Muuta että menee mallille
#* voit myös vain lisätä tietyt actionit mallille ja pitää loput kuten ennen
# pitääkö disabloida train vaiheessa? Eli fixata vain yksi mode? 
# ** ehkä en jaksa. Pitää vaan tietää mitä tekee
# ** Jos ei disabloi ja vaihtelee modesta toiseen niin vaihdoksen jälkeen pitää nollata kaikki kerätty train data.
# ** jos ei disabloituna, niin pitää tehdä current_mode jokaiselle environmentille erikseen.
# ** -> Joten disabloi


# Selvitä olisiko Unreal Engine parempi kuin pyBullet?
# * käy silmäilemässä pybullet esimerkkejä: https://github.com/bulletphysics/bullet3/tree/master/examples
# * MuJoCO:ssa voi antaa inertiafromgeom ja settotalmass. Ehkä urdf:ssäkin voi? Tai pyBulletille voi.

# TODO: Automatisoi inertian laskeminen. fuktio kopsattu urdf_utilsiin
# TODO: Jos 3 mallia niin voisiko opetella low level komennot vain kerran?

# TODO: älä vaihda tiettyjen steppien jälkeen vaan kun z error on pieni.

"""

lisää one-hot kokoa 3. 
arvo 50 välein uusi one hot
jos [0,0,1] -> ylös 
jos [1,0,0] -> alas 
jos [0,1,0] -> suoraan 

lisää agenttiin luokka, josta voi pyytää actionia.
* funktio ottaa inputiksi env id:t. Eli esim range len(envs) 
* rewardiin termi 

"""
#
#vertical takeoff:
#tee UserAction luokka
#* get_action: env_count kokonen vektori arvot 0,1,2
#** arvot pitää parsia one hotiksi ja lisätä prediction stateen 
#** action arvotaan uudelleen aina 80 välein
#* reward laskenta tarvitsee tiedon actionista. anna parametrinä.
#
#simulator.py
#* train: lisää collect_episode_data methodi joka rikastaa nyky staten seuraavalla actionilla.
#* not train: lisää action keyboard actionista

class EnvVars:
  def __init__(self, args):
    self.num_envs = args.num_envs
    self.steps_per_sec = 240 # pybulle default is 240Hz
    self.new_action_interval_s = 0.03
    self.simulations_per_step = int(self.new_action_interval_s * self.steps_per_sec)
    self.gravity = 9.81
    self.gravity_global_direction = np.array([0,0,-1])
    self.episode_length_s = 4 # 4 #+ env_vars.run_number/8
    # If full_episodes_cnt goes below 4 then learning becomes more unstable
    self.full_episodes_cnt = 5 # 5
    #self.steps_per_episode = self.get_steps_per_episode()
    self.max_times_to_be_updated = 50
    self.default_mode = Modes.fixed_mode # normal_mode fixed_mode
    self.allow_agent_actions = True
    self.random_forces = False
    self.headless = args.headless

    #self.randome_steps_at_begin_max = 15
    self.viscosity_multiplier = 1

    self.run_id = args.run_id
    self.run_number = args.run_number
    self.version = "t"+args.terminal
    self.mini_batch_size = 128
    self.ppo_epochs = 12
    self.debug = EasyDict({"KL": False, "env": False, "action_stat": False, "grad": False})
    self.start_height = 40
    self.lr = 0.0003
    self.info = HawkInfo()
    self.mode_info = EasyDict({
      Modes.fixed_mode: {"file": None, "random_steps_at_begin_max": 15},
      Modes.normal_mode: {"file": None, "random_steps_at_begin_max": 5}
    })
    
    #self.stabilizing_steps_begin = 0

    # TODO: jos lukee pretreenatun filen nini ehkä voisi overridetä run_id:n?
    #self.mode_info[Modes.fixed_mode].file = "scheduled/t1_20210219234934_-7.561_6875b9a5-94f8-41c0-a0c6-899f1edcb74a_9"
    
    # 512 self.mode_info[Modes.fixed_mode].file = "best/t1_20210219160524_-8.048_209ec076-7c09-4db3-a233-05569448ffd6"
    self.mode_info[Modes.fixed_mode].file = "best/t1_20210220191702_-3.037_d516d629-4086-43f3-9e68-2483338b1c1f"
    #self.mode_info[Modes.fixed_mode].file = "best/t1_20210221164350_-4.503_1c1133d5-0d65-429e-adfb-bd75a3222ad8"
    self.mode_info[Modes.fixed_mode].random_steps_at_begin_max = 0
    self.mode_info[Modes.normal_mode].random_steps_at_begin_max = 0
    #self.mode_info[Modes.fixed_mode].random_steps_at_begin_max = 0
    self.mode_info[Modes.normal_mode].file = "best/t1_20210206110235_-7.849_c8e21be4-5a9d-4b15-aa62-18689d7dcd42"
    #self.mode_info[Modes.normal_mode].file = "best/t1_20210206061255_-22.181_c3b8f872-3b21-4002-bf23-d407f8511c2f"
    #self.lr = 0.0001
    
    #self.allow_agent_actions = False
    #self.random_forces = True
    
    #self.run_number += 6
    self.ppo_epochs = 24
    # vanhin on action 0,1
    # toisiksi vanhin action -1,1
    # uusin on sama kuin edellä mutta 3*40 eikä 1*140

    self.episode_length_s = 15
    self.full_episodes_cnt = 15 #5
    #self.randome_steps_at_begin_max = 5
    self.start_height =  0.25 #130
    self.max_times_to_be_updated = 20

    self.writer = self.set_up_tensorboard(args)

  @property
  def train(self):
    #return True
    return self.headless

  @property
  def steps_per_episode(self):
    return int(self.episode_length_s / self.new_action_interval_s)

  def set_up_tensorboard(self, args):
    writer_path = "tensorboard_logs/"+datetime.now().strftime("%Y%m%d%H%M%S")
    writer = SummaryWriter(writer_path)
    start_time = strftime("%Y-%m-%d %H:%M:%S",gmtime())
    summary = f"run id {args.run_id}, run number {args.run_number}, learning rate {str(self.learning_rate)}, " + \
      f"start time {start_time}, episode length {self.episode_length_s}, episodes per update {self.full_episodes_cnt}, " + \
      f"version {self.version}"
    writer.add_text('run.py', summary, self.run_number)

    return writer

  def _newest_file(self, path):
    p = Path('./'+path).glob('*')
    files = [x for x in p if x.is_file()]
    return max(files, key=lambda x: x.stat().st_ctime).name

  @property
  def random_steps_at_begin(self):
    if random.random() < 0.3:
      return 0
    else:
      rnd_steps = self.mode_info[self.default_mode].random_steps_at_begin_max
      return random.randint(0,rnd_steps)
  
  @property
  def learning_rate(self):
    return self.lr / (1 + self.run_number*2)
  
  def update_mode_info(self, agent):
    mode_info = agent.mode_info

def make_env(i, env_vars, headless):
  def _thunk():
    env = Env(env_vars, i, not(not headless and i == 0))
    return env

  return _thunk

env_vars = EnvVars(args)
agent = Agent(env_vars)
env_vars.update_mode_info(agent)

envs = [make_env(i, env_vars, env_vars.headless) for i in range(args.num_envs)]
envs = SubprocVecEnv(envs)

print("Arguments: ", args)

simulator = Simulator(envs, env_vars, agent)
simulator.run()
