# Jos lisää seuraavan rivin niin control-c ei tuhoa koko prosessia
#!/bin/bash

# tensorboard --logdir=tensorboard_logs --reload_interval=15 --reload_multifile=True --window_title=Dragon --reload_multifile_inactive_secs=3600
# ctrl + shift + l    valitsee samannimisiä multi cursor editoitavaksi kaikki kerrallaan
# ctrl + d   valitsee samannimisiä multi cursor editoitavaksi yksi kerrallaan
# ps -ef | grep python
#Ctrl+Enter / Ctrl+Shift+Enter   --Insert line below/ above
# Ctrl+K Ctrl+C Add line comment
# Ctrl+K Ctrl+U Remove line comment

END=1
STATE="demo"

uuid=$(uuidgen)

TERMINAL="-t1"
if [ "$STATE" = "demo" ]
then
  PARALLELS="-e1"
  HEADLESS="-d false"
else
  PARALLELS="-e10"
  HEADLESS="-d true"
fi

x=$END
while [ $x -gt 0 ]; 
do
  echo "${x}"
  RUNNUMBER=$(expr $END - $x)
  eval "python run.py ${TERMINAL} ${PARALLELS} ${HEADLESS} -r ${RUNNUMBER} -u ${uuid}" 
  x=$(($x-1))
done

