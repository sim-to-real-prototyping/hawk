import pybullet as p
import time
import numpy as np
import math
from easydict import EasyDict
import time
from physics_simulator import Physics
from vehicle.hawk_info import HawkInfo
from random_force import RandomForces
from vehicle.state_history import VehicleStateHistory
from vehicle.motor_state import MotorState
# http://kieranwynn.github.io/pyquaternion
#from pyquaternion import Quaternion


# Static functions that need pybullet library. Should not be used by Env,Simulator
class HawkStateUtils:
  # TODO: PhysicSimulator angle_between()
  #@staticmethod
  #def _get_angle_between_AB(A, B):
  #  print(A, B, "ttttttt")
  #  # spin axis so that B is x,y
  #  matrix = [B, [-B[1],B[0]]]
  #  Q = np.array(matrix).T
  #  an = np.linalg.solve(Q, A)
  #  radians = math.atan2(an[1],an[0])
  #  return radians

  @staticmethod
  def get_id(name, vehicle_id):
    code = HawkInfo.name_to_code[name]
    return HawkStateUtils.code_to_id(code, vehicle_id)

  @staticmethod
  def code_to_id(code, vehicle_id):
    if code == p.getBodyInfo(vehicle_id)[0].decode("utf-8"):
      raise Exception("ei pitäisi käydä tässä")
      #return -1

    for _id in range(p.getNumJoints(vehicle_id)):
      link_code = p.getJointInfo(vehicle_id, _id)[12].decode('UTF-8')
      joint_code = p.getJointInfo(vehicle_id, _id)[1].decode('UTF-8')
      if code == link_code or code == joint_code:
        return _id

    return None


class HawkState(HawkStateUtils):
  def __init__(self, env_vars, vehicle_id, info):
    HawkStateUtils.__init__(self)
    self.env_vars = env_vars
    self.steps_per_sec = env_vars.steps_per_sec
    self.vehicle_id = vehicle_id
    self.info = info
    self.gravity = env_vars.gravity
    self.motor_state = MotorState(env_vars)
    self.hist = VehicleStateHistory(self.info.vehicle_codes, self.info.wing_codes, env_vars.steps_per_sec)

    link_ids = [self.code_to_id(l, self.vehicle_id) for l in self.info.wing_codes + self.info.fuselag_codes]
    self.randome_forces = RandomForces(link_ids, vehicle_id, env_vars.random_forces)

  def _gravity_angle(self):
    x,y,z = self.gravitation_in_local_coordinate()
    # is minus z to make gravity directly below being 0 and if upside down then +/-pi.
    xz = math.atan2(x,-z)
    yz = math.atan2(y,-z)
    return xz, yz

  def _force_to_motor(self):
    def update_joint_val(joint_name):
      joint_torque = self.info.motor_force.max_torque
      target = self.motor_state.targets[joint_name]

      current = self.motor_state.current_values[joint_name]
      MINIMUM_JOINT_DELTA = 0.02
      if abs(current - target) > MINIMUM_JOINT_DELTA:
        self.set_joint_to(target, joint_name, joint_torque)
        self.motor_state.current_values[joint_name] = target

    t = self.motor_state.targets
    front_motor = self.code_to_id("front_motor", self.vehicle_id)
    p.applyExternalForce(self.vehicle_id, front_motor, [0,0, t.front], [0,0,0], p.LINK_FRAME)
    
    back_left_motor = self.code_to_id("back_left_motor", self.vehicle_id)
    p.applyExternalForce(self.vehicle_id, back_left_motor, [0,0,t.back_left], [0,0,0], p.LINK_FRAME)
    
    back_right_motor = self.code_to_id("back_right_motor", self.vehicle_id)
    p.applyExternalForce(self.vehicle_id, back_right_motor, [0,0,t.back_right], [0,0,0], p.LINK_FRAME)
    
    update_joint_val("front_pitch")
    update_joint_val("front_roll")
    update_joint_val("back_pitch")
    update_joint_val("back_roll")

  # TODO: luonnonilmiöin laskenta pois hawkstatesta
  def add_viscosity_forces(self, code, link_type, code_info=None):
    def get_angle_of_attack(speed_vec_local):
      # [1,0] since zero local diff is [0,1,0] xyz vector
      angle_of_attack = Physics.angle_between([1,0], [speed_vec_local[1], speed_vec_local[2]])
      return -np.sign(speed_vec_local[2])*angle_of_attack

    def get_speed_speedvec_dragarea(code):
      if len(self.hist.positions[code]) < 10:
        return 0, [0,1,0], [0,0,0], np.array([0,1,0])
      else:
        speed, speed_vec = self._get_speed_info(code)
        speed_vec_local = self._map_to_code_coord(speed_vec, code)
        drag_areas = Physics.drag_area(speed_vec_local, self.info.dimensions[code]["dims_xyz"], code_info)
 
        if np.absolute(speed_vec_local).sum() > 200:
          raise Exception("So big speed components, that there might be a bug")

        return speed, speed_vec, drag_areas, speed_vec_local

    speed, speed_vec, drag_areas, speed_vec_local = get_speed_speedvec_dragarea(code)
    speed_vec_local_unit = Physics.unit_vector(speed_vec_local)

    if len(self.hist.positions[code]) < 10:
      angle_of_attack = 0
      drag_coeffs = np.repeat([0.5],3)
      if link_type == "wing":
        coeff = 0
    else:
      if link_type == "wing":
        angle_of_attack = get_angle_of_attack(speed_vec_local)
        coeff = Physics.lift_coeff(angle_of_attack, speed_vec_local_unit)
        drag_coeffs = Physics.drag_coeff_lift(angle_of_attack, coeff, self.info.dimensions[code]["dims_xyz"])
        drag_coeffs= np.repeat(drag_coeffs, 3)
      elif link_type == "fuselag":
        drag_coeffs = Physics.drag_coeff_body(speed_vec_local_unit)
      elif link_type == "rudder":
        drag_coeffs = Physics.drag_coeff_body(speed_vec_local_unit)
    
    drag_force = Physics.get_drag(drag_coeffs, speed_vec_local, drag_areas)
    drag_force = np.array(drag_force)

    self.hist.prev_drag[code] = drag_force*self.env_vars.viscosity_multiplier
    if np.absolute(self.hist.prev_drag[code]).sum() > 50:
      raise Exception("So big drag force that probably a bug")

    self.hist.prev_velocity[code] = speed_vec

    if link_type == "wing":
      area = self._area(code)
      lift = Physics.get_lift(area, speed, coeff)
      self.hist.prev_lift[code] = lift*self.env_vars.viscosity_multiplier

  def _update_state_values(self):
    for code in self.info.wing_grid_codes:
      self.add_viscosity_forces(code, "wing", self.info.wing_grid_info[code])
    
    for code in self.info.fuselag_codes:
      self.add_viscosity_forces(code, "fuselag")

    for code in self.info.rudder_codes:
      self.add_viscosity_forces(code, "rudder")

  # TODO: luonnonilmiöin laskenta pois hawkstatesta
  def _update_lift_drag(self):
    # Drag force applies to the suface of link. And only to the velocity size surface.
    def update_drag(code, code_id):
      drag = self.hist.prev_drag[code]
      dims = np.array(self.info.dimensions[code]["dims_xyz"]) / 2
      link_pos_center = [0,0,0]
      p.applyExternalForce(self.vehicle_id, code_id, drag, link_pos_center, p.LINK_FRAME)

    def update_lift(code, code_id):
      lift = self.hist.prev_lift[code]
      p.applyExternalForce(self.vehicle_id, code_id, [0,0,lift], [0,0,0], p.LINK_FRAME)

    for code in self.info.wing_grid_codes:
      if code not in self.hist.prev_drag:
        continue
      code_id = self.code_to_id(code, self.vehicle_id)
      update_drag(code, code_id)
      update_lift(code, code_id)
    
    for code in self.info.fuselag_codes:
      if code not in self.hist.prev_drag:
        continue
      code_id = self.code_to_id(code, self.vehicle_id)
      update_drag(code, code_id)

  def update_position_info(self):
    for code in self.info.vehicle_codes:
      world_pos_center, world_ori_center = self._pos_orient(code)
      self.hist.add_position(world_pos_center, code)

    return self.hist.is_actionable_pos_hist(self.info.fuselag_codes[0])
    
  def presimulation_step(self, action, full_render):
    self.motor_state.set_motor_states(action)

    self._force_to_motor()
    self.randome_forces.step()
    if full_render:
      self._update_state_values()

    self._update_lift_drag()

    gravity_loc = self.gravitation_in_local_coordinate()
    self.hist.append_local_gravity_direction(gravity_loc)

  def _pos_orient(self, code):
    id = self.code_to_id(code, self.vehicle_id)
    if id == -1:
      raise Exception("Base object was behaving unexpectedly. So base object should not be used in position, orientation or force calculations")
    else:
      state = p.getLinkState(self.vehicle_id, id)

    world_pos_center, world_ori_center = [state[i] for i in [0,1]]
    if np.isnan(world_pos_center).any():
      raise Exception("World position contains nan values")

    return world_pos_center, world_ori_center

  def _area(self, wing_name):
    x,y,z = self.info.dimensions[wing_name]["dims_xyz"]
    return x*y

  def _below_unit(self, code):
    below_loc_name =  code + "_below"
    world_pos_center, world_ori_center = self._pos_orient(code)
    world_pos_below, world_ori_below = self._pos_orient(below_loc_name)
    vec_below = np.array(world_pos_below) - np.array(world_pos_center)
    vec_below = Physics.unit_vector(vec_below)
    return vec_below

  def _comp_front(self, code):
    front_loc_name =  code + "_front"
    world_pos_center, world_ori_center = self._pos_orient(code)
    world_pos_front, world_ori_front = self._pos_orient(front_loc_name)
    vec_front = np.array(world_pos_front) - np.array(world_pos_center)
    return vec_front

  def _front_unit(self, code):
    vec_front = Physics.unit_vector(self._comp_front(code))
    return vec_front
  
  def _right_unit(self, code):
    right_loc_name =  code + "_right"
    world_pos_center, world_ori_center = self._pos_orient(code)
    world_pos_front, world_ori_right = self._pos_orient(right_loc_name)
    vec_right = np.array(world_pos_front) - np.array(world_pos_center)
    vec_right = Physics.unit_vector(vec_right)
    
    return vec_right

  def _projection_to_matrix_coord(self, matrix, vec2):
    Q = np.array(matrix).T
    return np.linalg.solve(Q, vec2)

  @staticmethod
  def _assert_reference_link_angles_correct(up, front, right):
    """
    There was a bug. Using base link as part of angle calculation did not give 90 degrees angles with front, up and right. 
    This method is to verify such issue will be catched earlier next time.
    """
    def assert_angle(a,b):
      dot_product = np.dot(a,b)
      assert np.absolute(np.arccos(dot_product) - math.pi/2) < 0.001

    if np.random.rand() < 0.01:
      assert_angle(up, front)
      assert_angle(up, right)
      assert_angle(right, front)

  def _map_to_code_coord(self, other_vec, code):
    up_vec = -1*self._below_unit(code) # TODO: Tämä pitää olla oikeasti +1 eikä -1
    front_vec = self._front_unit(code)
    right_vec = self._right_unit(code) # TODO: varmista, että right antaa +1 eikä -1
    self._assert_reference_link_angles_correct(up_vec, front_vec, right_vec)
    return self._projection_to_matrix_coord([right_vec, front_vec, up_vec], other_vec)        

  def _map_to_infuselag2_coord(self, other_vec):
    return self._map_to_code_coord(other_vec, "fuselag2")
  
  # Calculates fuselag 2 velocity projected to fuselag2 coordinate
  def _velocity_in_local_coordinate(self):
    speed, speed_vec = self._get_speed_info("fuselag2")
    return self._map_to_infuselag2_coord(speed_vec), speed

  def gravitation_in_local_coordinate(self, gravity_glo = np.array([0,0,-1])):
    """
    Returns gravitation direction from fuselag2. Returned direction is 3d vector. Values are shperical unit 
    coordinates relative to y-axis [0,1,0]
    """
    return self._map_to_infuselag2_coord(gravity_glo)

  def non_gravity_acceleration(self):
    return self.non_grav_acce("fuselag2", 30)

  def non_grav_acce(self, code, over_last_n_steps = 10):
    
      acceleration_per_frame = self.hist.non_grav_acce_per_frame(code, over_last_n_steps)
      # derived from https://en.wikipedia.org/wiki/Acceleration
      # s(t) = s0 + v0*t + 0.5a*t²
      # Probably not exactly accurate formual, but hopefully close enough. For some reason seems to give correct values without multiplier 2.
      # Scaled by earth graviation. Maybe should be removed at some point
      acceleration_norm = acceleration_per_frame*self.steps_per_sec*self.steps_per_sec/self.gravity
      return acceleration_norm

  def _get_speed_info(self, code):
    speed_vec = self.hist.get_global_speed_vec(code)
    if speed_vec is None:
      return 0, [0,0.001,0]
    speed = np.linalg.norm(speed_vec)
    return speed, speed_vec

  def get_height(self):
    world_pos, world_ori = self._pos_orient("fuselag2")
    return world_pos[2]
      
  def state_info(self):
    code = "wing1"
    world_pos, world_ori = self._pos_orient(code)
    # TODO: Käytänköhän väärin termejä pitch,roll,yaw
    pitch,roll,yaw = p.getEulerFromQuaternion(world_ori)

    speed = self.hist.get_speed(code)
    
    return world_pos,pitch,roll,yaw,speed
 
  #def _get_limits(self, code):
  #  id = self.get_id(code, self.vehicle_id)
  #  return p.getJointInfo(self.vehicle_id, id)[8:10]

  def get_state(self):
    state = self.info.get_nan_state()

    state_fields = self.info.state_fields

    state[state_fields.front_pitch] = self._get_named_joint_state("front_pitch")
    state[state_fields.front_roll] = self._get_named_joint_state("front_roll")
    state[state_fields.back_pitch] = self._get_named_joint_state("back_pitch")
    state[state_fields.back_roll] = self._get_named_joint_state("back_roll")
    
    t = self.motor_state.targets
    state[state_fields.front] = t.front
    state[state_fields.back_left] = t.back_left
    state[state_fields.back_right] = t.back_right

    speed_vec, speed = self._velocity_in_local_coordinate()
    airspeed_in_fuselag2_direction = speed_vec[1]
    state[state_fields.speed_fus2_dir] = airspeed_in_fuselag2_direction
    state[state_fields.speed] = speed

    x_loc, y_loc, z_loc = self.gravitation_in_local_coordinate()

    state[state_fields.x_in_local_gravity] = x_loc
    state[state_fields.y_in_local_gravity] = y_loc
    state[state_fields.z_in_local_gravity] = z_loc

    gravity_angle_xz, gravity_angle_yz = self._gravity_angle()
    state[state_fields.gravity_angle_xz] = gravity_angle_xz
    state[state_fields.gravity_angle_yz] = gravity_angle_yz

    recent_gravity_angle_xz_avg, recent_gravity_angle_yz_avg = self.hist.get_recent_gravity_angle(gravity_angle_xz, gravity_angle_yz)
    state[state_fields.recent_gravity_angle_xz_avg] = recent_gravity_angle_xz_avg
    state[state_fields.recent_gravity_angle_yz_avg] = recent_gravity_angle_yz_avg


    state[state_fields.front_motor_vec] = self._comp_front("front_motor").tolist()
    state[state_fields.back_motor_vec] = self._comp_front("back_left_motor").tolist()
    
    state[state_fields.fuselage2_front] = self._front_unit("fuselag2") #self._comp_front("back_left_motor").tolist()

    state[state_fields.height] = self.get_height()
    CHANGE_OVER_TIMESTEPS = 10
    state[state_fields.height_delta] = self.hist.height_change(CHANGE_OVER_TIMESTEPS)

    state[state_fields.horizonta_movement_latest] = self.hist.avg_horizontal_movement_ts_ago(self.env_vars.simulations_per_step,1, "fuselag2")
    
    for key in state:
      if np.isnan(state[key]).any():
        raise Exception("State has nan values")
          
    return state
      
  def _get_named_joint_state(self, name):
    id = self.get_id(name, self.vehicle_id)
    return p.getJointState(self.vehicle_id,id)[0]

  def set_joint_to(self, val, name, joint_torque):
    id = self.get_id(name, self.vehicle_id)
    p.setJointMotorControl2(self.vehicle_id,id,p.POSITION_CONTROL,targetPosition=val,force=joint_torque)

