#import numpy as np
from easydict import EasyDict
import xml.etree.cElementTree as ET
import numpy as np
from vehicle.urdf_util import add_wing_grid_to_urdf, links_to_ignore

# Contains static information about vehicle. 
class HawkInfo:
  wing_segments = 1

  wing_codes = ["wing1", "wing2", "wing3"]
  fuselag_codes = ["fuselag1", "fuselag2", "fuselag3_1", "fuselag3_2"]
  rudder_codes = ["rudder"]
  fuselag_joint_codes = ["fuselag2a", "fuselag2b"]
  original_urdf = "urdfs/hawk.urdf"
  file_path = './urdfs/hawk.urdf'
  generated_urdf = "./urdfs/generated.urdf"

  name_to_code = {"front_pitch": "fuselag_1b_to_1c",
                  "front_roll": "fuselag_1c_to_2a",
                  "back_pitch": "fuselag_31b_to_31c", 
                  "back_roll": "fuselag_31c_to_32a"}

  vertical = EasyDict({ "back_roll": {"start": 0, "edit": False},
                        "back_pitch":{"start": 1.6, "edit": False},
                        "front_roll": {"start": 0, "edit": True},
                        "front_pitch":{"start": -1.65, "edit": True},
                        "front": {"start": 5.86, "edit": True},# 5.86
                        "back": {"start": 3.9, "edit": True}}) # 3.9

  state_field_names = ["gravity_angle_xz", "gravity_angle_yz", "recent_gravity_angle_xz_avg", "recent_gravity_angle_yz_avg",
    "speed_fus2_dir","x_in_local_gravity","y_in_local_gravity","z_in_local_gravity","front", "back_left", 
    "back_right", "front_pitch", "front_roll", "back_pitch", "back_roll", "front_motor_vec", "back_motor_vec", "height",
    "height_delta", "horizonta_movement_latest", "speed", "fuselage2_front"]

  def __init__(self):

    #TODO: luo generated.urdf
    # tee funkkari joka palauttaa varsinaiset linkit. ilman ref pisteitä tai basea.
    # poista get_link_info. Tai vähintään siirrä se utils luokkaan.
    # siirrä self.wing_grid_info urdf_utils luokkaan.
    self.wing_grid_info = add_wing_grid_to_urdf(self.original_urdf, self.generated_urdf)
    self.wing_grid_codes = list(self.wing_grid_info.keys())

    self.vehicle_codes = self.wing_grid_codes + self.fuselag_codes + self.fuselag_joint_codes + self.rudder_codes

    upper_force_front_engine = 15 # N m/s
    self.delta_force = upper_force_front_engine / 20
    # TODO: Torque is necessarily effective. joint effort is defined in urdf -file.
    # Unit is N m/s
    self.motor_force = EasyDict({ "front": {"upper": upper_force_front_engine, "lower": -self.delta_force}, 
                                  "back": {"upper": 7, "lower": -self.delta_force/2},
                                  "max_torque": 5})

    code_to_name = {}
    for k, v in self.name_to_code.items():
      code_to_name[v] = k

    self.code_to_name = code_to_name
    self.joint_info = self.get_joint_upper_lower(code_to_name)

    front_pitch_abs_lim = min(abs(self.joint_info.front_pitch.upper), abs(self.joint_info.front_pitch.lower))
    back_pitch_abs_lim = min(abs(self.joint_info.back_pitch.upper), abs(self.joint_info.back_pitch.lower))
    self.horizontal = EasyDict({ "back_roll": {"start": 0, "edit": True},
                            "back_pitch":{"start": 0, "edit": True, "max_abs": back_pitch_abs_lim},
                            "front_roll": {"start": 0, "edit": True},
                            "front_pitch":{"start": 0, "edit": True, "max_abs":front_pitch_abs_lim},
                            "front": {"start": 0, "edit": True},
                            "back": {"start": 0, "edit": True}})

    self.dimensions, self.masses = self.get_link_info()
    # overrides old wings with segment
    dims = []
    for seg_key in self.wing_grid_info:
      self.dimensions[seg_key] = {"dims_xyz":self.wing_grid_info[seg_key]["dims"], "shape": "box"}

    self.state_fields = self._get_state_fields()

  @staticmethod
  def get_nan_state():
    state = {}
    for field in HawkInfo.state_field_names:
      state[field] = np.nan
    #state = np.empty(len(HawkInfo.state_field_names))
    #state[:] = np.nan
    return state 

  @staticmethod
  def _get_state_fields():
    state_fields = {}
    for name in HawkInfo.state_field_names:
      state_fields[name] = name

    return EasyDict(state_fields)

  # TODO: pitäskö tämä yhdistää urdf_utilsin kanssa?
  @staticmethod
  def get_link_info():
    root = ET.parse('./urdfs/hawk.urdf').getroot()
    dimensions = {}
    masses = {}
    for type_tag in root.findall('link'):
      inertial = type_tag.findall('inertial')
      assert len(inertial) == 1
      mass = inertial[0].findall('mass')
      assert len(mass) == 1
      mass = float(mass[0].get("value"))

      value = type_tag.get('name')
      if value in links_to_ignore:
        continue

      visual = type_tag.findall('visual')
      assert len(visual) == 1
      geometry = visual[0].findall("geometry")
      assert len(geometry) == 1
      box = geometry[0].findall("box")
      if len(box) == 1:
        sizes = box[0].get("size")
        # width, length, height
        dim = list(map(float, sizes.split(" ")))
        area = dim[0]*dim[1] 
        dimensions[value] = {"dims_xyz":dim, "shape": "box"}
        masses[value] = mass

      else:
        assert len(box) == 0
        cylinder = geometry[0].findall("cylinder")
        assert len(cylinder) == 1
        radius = float(cylinder[0].get("radius"))
        length = float(cylinder[0].get("length"))
        
        dimensions[value] = {"radius":radius, "length": length, "shape": "cylinder"}
        masses[value] = mass

    return dimensions, masses

  @staticmethod
  def get_joint_upper_lower(code_to_name):
    root = ET.parse(HawkInfo.file_path).getroot()
    joint_info = {}

    for type_tag in root.findall('joint'):
      value = type_tag.get('type')
      if value == "revolute":

        limits = type_tag.findall('limit')
        assert len(limits) == 1
        joint_info[code_to_name[type_tag.get('name')]] = {"upper": float(limits[0].get("upper")), 
                                                          "lower": float(limits[0].get("lower")), 
                                                          "code": type_tag.get('name')}

    return EasyDict(joint_info)

  @staticmethod
  def empty_actions():
    return EasyDict({"front": 0,"back_left": 0,"back_right":0,"back_roll":0,"back_pitch":0,"front_roll": 0,"front_pitch":0})


