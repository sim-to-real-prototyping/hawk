import pybullet as p
from vehicle.hawk_state import HawkState, HawkStateUtils
#from vehicle.hawk_info import HawkInfo

class HawkFactory:
  @staticmethod
  def create_vehicle(env_vars):
    vehicle_id = p.loadURDF(env_vars.info.generated_urdf)
    HawkFactory._start_position(vehicle_id, env_vars.start_height)

    return HawkState(env_vars, vehicle_id, env_vars.info)

  @staticmethod
  def _start_position(vehicle_id, start_height):
    #x,y,z = 0,0,0.25
    #x,y,z = 0,0,150
    x,y,z = 0,0,start_height
    roll,pitch,yaw = 0,0,0 #1.4,0,0 # roll pitäisi olla pitch?
    q=p.getQuaternionFromEuler((roll,pitch,yaw))

    p.resetBasePositionAndOrientation(vehicle_id,(x,y,z),q)
    p.resetJointState(vehicle_id, HawkStateUtils.get_id("front_pitch", vehicle_id), 0) # 1-.65
    p.resetJointState(vehicle_id, HawkStateUtils.get_id("back_pitch", vehicle_id), 0) # 1.6
