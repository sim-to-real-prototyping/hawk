import elementpath
from xml.dom import minidom
from xml.etree import ElementTree as ET

links_to_ignore = ["baselink"]

# TODO: Automatisoi inertian laskeminen
# https://github.com/gstavrinos/calc-inertia/blob/master/calc_inertia.py
#def getBoxInertia(x, y, z, m, s):
#    xx = 1./12 * m * (y**2 + z**2) * s
#    yy = 1./12 * m * (x**2 + z**2) * s
#    zz = 1./12 * m * (x**2 + y**2) * s
#    return xx, yy, zz
#

def add_wing_grid_to_urdf(original_file, generated_file):
  def get_link_info(root):
    link_info = []
    for type_tag in root.findall('link'):
      link_code = type_tag.get('name')
      if link_code in links_to_ignore:
        continue

      collision = type_tag.findall('collision')
      assert len(collision) == 1
      geometry = collision[0].findall("geometry")
      assert len(geometry) == 1
      box = geometry[0].findall("box")
      if len(box) == 1:
        sizes = box[0].get("size")
        dim = list(map(float, sizes.split(" ")))
        link_info.append({"dims":dim,"code":link_code})
    return link_info

  def get_wing_dims(link_info, wing_codes):
    wing_dimensions = {}
    for datum in link_info:
      if datum["code"] in wing_codes:
        wing_dimensions[datum["code"]] = datum["dims"]
    return wing_dimensions

  def get_joint_x_y_z(postfix):
    z_to_parent = 0
    y_to_parent = 0
    x_to_parent = 0
    if postfix == "below":
      z_to_parent = -0.05
    elif postfix == "front":
      y_to_parent = 0.05
    elif postfix == "right":
      x_to_parent = 0.05
    elif postfix is not None:
      raise Exception(f"""Given "{postfix}" postfix not defined""")

    return x_to_parent, y_to_parent, z_to_parent
      
  def get_joint_xyz(postfix):
    x,y,z = get_joint_x_y_z(postfix)
    return f"{x} {y} {z}"


  def get_segment_joint_xyz(postfix, index, segment_x, wing_segments_count):
    x_to_parent, y_to_parent, z_to_parent = get_joint_x_y_z(postfix)
      
    relative_pos_x = segment_x*(index - wing_segments_count) + x_to_parent
    relative_pos_to_parent_urdf = f"{relative_pos_x} {y_to_parent} {z_to_parent}"
    return relative_pos_to_parent_urdf

  def geometry_tag():
    geometry = ET.Element("geometry")
    box = ET.SubElement(geometry, "box", {"size": "0.0001 0.0001 0.0001"})
    return geometry

  def create_link(name):
    link = ET.Element("link", {"name": name})
    visual = ET.SubElement(link,"visual")
    visual.append(geometry_tag())
    collision = ET.SubElement(link,"collision")
    collision.append(geometry_tag())
    inertial = ET.SubElement(link,"inertial")
    mass = ET.SubElement(inertial,"mass", {"value":"0"})
    inertia = ET.SubElement(inertial,"inertia", {"ixx":"0.000001", "ixy":"0", "ixz":"0", "iyy":"0.000001", "iyz":"0", "izz":"0.000001"})
    return link

  def add_new_urdf_elements(root, wing_segments, link_info):
    existing_link_codes = [c["code"] for c in link_info]
    for segment_key in wing_segments.keys():
      segment = wing_segments[segment_key]
      if segment_key != segment["parent_link"]:
        link = create_link(segment_key)

        joint_name = segment["parent_link"] + "_to_" + segment_key
        joint = ET.Element("joint", {"name": joint_name, "type":"fixed"})
        parent = ET.SubElement(joint,"parent", {"link": segment["parent_link"]})
        child = ET.SubElement(joint,"child", {"link": link.get('name')})
        origin = ET.SubElement(joint,"origin", {"xyz":segment["urdf_joint_xyz"], "rpy":"0 0 0"})

        if segment_key in existing_link_codes:
          raise Exception(f"Key {segment_key} already exists")

        root.append(link)
        root.append(joint)

    return root

  def new_segment_info(wing_dimensions, wing_codes, postfix = None):
    wing_segments_count = 1
    segment_count = wing_segments_count*2+1
    wing_segments = {}

    for wing_code in wing_codes:
      x,y,z = wing_dimensions[wing_code]
      segment_x = x/segment_count
      for index in range(segment_count):
        #piece_code = True if index == 0 or index == segment_count - 1 else False
        if index == 0:
          piece_code = -1
        elif index == segment_count - 1:
          piece_code = 1
        else:
          piece_code = 0

        if wing_segments_count==index:
          segment_code = wing_code
        else:
          pf = "" if postfix is None else "_" + postfix
          segment_code = f"{wing_code}_{index}{pf}"

        dims = segment_x, y, z

        relative_pos_to_parent_urdf = get_segment_joint_xyz(postfix, index, segment_x, wing_segments_count)
        wing_segments[segment_code] = {"dims":dims, "parent_link": wing_code, \
          "urdf_joint_xyz": relative_pos_to_parent_urdf, "piece_code": piece_code}

    return wing_segments

  def add_directions_comps(codes):
    new_comp = {}
    for postfix in ["below", "front", "right"]:
      for code in codes:
        segment_code = f"{code}_{postfix}"
        urdf_joint_xyz = get_joint_xyz(postfix)
        new_comp[segment_code] = {"parent_link": code, \
            "urdf_joint_xyz": urdf_joint_xyz}

    return new_comp

  def load_root(file_name):
    et_tree = ET.parse(file_name)
    return et_tree.getroot()

  def save_root(root, file_name):
    xmlstr = minidom.parseString(ET.tostring(root)).toprettyxml(indent="  ")
    xmlstr = "\n".join([ll.rstrip() for ll in xmlstr.splitlines() if ll.strip()])
    with open(file_name, "w") as f:
      f.write(xmlstr)

  root = load_root(original_file)
  wing_codes = ["wing1", "wing2", "wing3"]
  link_info = get_link_info(root)

  wing_dimensions = get_wing_dims(link_info, wing_codes)
  wing_segments = new_segment_info(wing_dimensions, wing_codes)

  root = add_new_urdf_elements(root, wing_segments, link_info)

  wing_segments_below = new_segment_info(wing_dimensions, wing_codes, "below")
  root = add_new_urdf_elements(root, wing_segments_below, link_info)
  
  wing_segments_front = new_segment_info(wing_dimensions, wing_codes, "front")
  root = add_new_urdf_elements(root, wing_segments_front, link_info)

  wing_segments_right = new_segment_info(wing_dimensions, wing_codes, "right")
  root = add_new_urdf_elements(root, wing_segments_right, link_info)

  dir_codes = add_directions_comps(wing_codes + ["fuselag1", "fuselag2", "fuselag3_1", "fuselag3_2", "rudder"])
  root = add_new_urdf_elements(root, dir_codes, link_info)

  save_root(root, generated_file)
  return wing_segments

