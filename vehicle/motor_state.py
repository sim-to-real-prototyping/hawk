from easydict import EasyDict
import numpy as np
import copy
from models.modes import Modes


# TODO: remove this class?
class MotorState:
  def __init__(self, env_vars):
    info = env_vars.info
    self.delta_force = info.delta_force
    if env_vars.default_mode == Modes.fixed_mode:
      h = info.vertical
    elif env_vars.default_mode == Modes.normal_mode:
      h = info.horizontal
    else:
      raise Exception("Mode not recognized")

    self.targets = EasyDict({ "front": h.front.start,
                              "back_left": h.back.start,
                              "back_right":h.back.start,
                              "back_roll":h.back_roll.start,
                              "back_pitch":h.back_pitch.start,
                              "front_roll": h.front_roll.start,
                              "front_pitch":h.front_pitch.start})
    self.current_values = info.empty_actions()

  def set_motor_states(self, state):
    self.targets.front = state["front"]
    self.targets.back_left = state["back_left"]
    self.targets.back_right = state["back_right"]

    self.targets.front_pitch = state["front_pitch"]
    self.targets.front_roll = state["front_roll"]
    self.targets.back_pitch = state["back_pitch"]
    self.targets.back_roll = state["back_roll"]

