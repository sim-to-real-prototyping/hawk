from easydict import EasyDict
import numpy as np

class VehicleStateHistory:
  def __init__(self, vehicle_codes, wing_names, steps_per_sec):
    self.steps_per_sec = steps_per_sec
    self.MIN_OLD_POS_LEN = 4
    self.positions = {}
    for code in vehicle_codes:
      self.positions[code] = []

    self.prev_gravity_local_directions = [0,0,0]
    self.prev_lift = {}
    self.prev_drag = {}
    self.prev_velocity = {}

    for wing_name in wing_names:
      self.prev_lift[wing_name] = 0
      self.prev_velocity[wing_name] = [0,0,0]

    self.gravity_dir_hist = []
    self.gravity_angles = EasyDict({"xz":[], "yz":[]})
  
  def avg_horizontal_movement_ts_ago(self, from_steps, to_steps, code):
    assert to_steps < from_steps
    if len(self.positions[code]) < from_steps:
      return (0,0)
    
    start_pos = self.positions[code][-from_steps]
    end_pos = self.positions[code][-to_steps]

    diff = np.array(end_pos) - np.array(start_pos)
    diff /= (from_steps - to_steps)
    return diff[0], diff[1]

  def non_grav_acce_per_frame(self, code, over_last_n_steps):
    if len(self.positions[code]) < self.MIN_OLD_POS_LEN:
      raise Exception("Code run should not come here")
    else:
      # Calculate avg acceleration over last N frames
      accelerations = []
      stest_to_take = min(len(self.positions[code]), over_last_n_steps)
      
      for i in np.arange(1,stest_to_take-1,4):
        vel = np.array(self.positions[code][-i]) - np.array(self.positions[code][-i-1])
        old_vel = np.array(self.positions[code][-i-1]) - np.array(self.positions[code][-i-2])
        accelerations.append(vel - old_vel)
        
      return np.array(accelerations).mean(axis=0)

  def get_global_speed_vec(self, code):
    if len(self.positions[code]) < 2:
      return None
    start_pos = self.positions[code][-2]
    end_pos = self.positions[code][-1]
    speed_vec = (np.array(end_pos) - np.array(start_pos))*self.steps_per_sec

    return speed_vec

  def get_speed(self, code):
    speed_vec = self.get_global_speed_vec(code)
    return 0 if speed_vec is None else np.linalg.norm(speed_vec)

  def get_recent_gravity_angle(self, xz, yz):
    self.gravity_angles.xz.append(xz)
    self.gravity_angles.yz.append(yz)
    
    if len(self.gravity_angles.xz) > 5:
      self.gravity_angles.xz = self.gravity_angles.xz[-5:]
      self.gravity_angles.yz = self.gravity_angles.yz[-5:]

    return np.array(self.gravity_angles.xz).mean(),np.array(self.gravity_angles.yz).mean()
    
  def prev_local_gravitation(self):
    if self.gravity_dir_hist == []:
      return [0,0,-1]
    
    return self.gravity_dir_hist[-1]

  # Code is fuselag2
  def append_local_gravity_direction(self, direction): 
    self.gravity_dir_hist.append(direction)
    if len(self.gravity_dir_hist) > 3:
      self.gravity_dir_hist = self.gravity_dir_hist[1:]

  def add_position(self, world_pos_center, code):
    if np.isnan(world_pos_center).any():
      raise Exception("Adding nan position")
    self.positions[code].append(list(world_pos_center))
    if len(self.positions[code]) > self.steps_per_sec:
      self.positions[code] = self.positions[code][1:]

  def is_actionable_pos_hist(self, code):
    return len(self.positions[code]) >= self.MIN_OLD_POS_LEN
    
  def height_change(self, over_ts, code="fuselag2"):
    pos = self.positions[code]
    if pos == []:
      return 0
    elif over_ts > len(pos):
      return pos[-1][2] - pos[0][2]
    else:

      return pos[-1][2] - pos[-1 - over_ts][2]