import pybullet as p
import numpy as np
import time
from easydict import EasyDict
import random
import math 

from vehicle.hawk_factory import HawkFactory
from keyboards.system_control import KeyboardActions

class DataInfo:
  def __init__(self, shape):
    self.shape = shape

class Env:
  def __init__(self,env_vars, env_id, headless):
    self.env_vars = env_vars
    self.env_id = env_id
    self.random_forces = env_vars.random_forces
    self.gravity = env_vars.gravity
    self.new_action_interval_s = env_vars.new_action_interval_s

    self.steps_per_sec = env_vars.steps_per_sec
    self.episode_length_s = env_vars.episode_length_s #+ env_vars.run_number/8
    if env_vars.debug.env:
      print("Episode length", self.episode_length_s)

    self.env_step_count = 0
    self.p_simulation_steps = 0

    self.headless = headless
    self.gravity_glo = np.array([0,0,-1])
    #self.steps_per_episode = env_vars.steps_per_episode

    self.simulations_per_step = env_vars.simulations_per_step #int(self.new_action_interval_s * self.steps_per_sec)

    if headless:
      p.connect(p.DIRECT)
    else:
      p.connect(p.GUI)

    plane = p.createCollisionShape(p.GEOM_PLANE)
    p.createMultiBody(0, plane)
    p.setRealTimeSimulation(0)
    #p.setRealTimeSimulation(True)
    p.configureDebugVisualizer(p.COV_ENABLE_MOUSE_PICKING, 1)

    #p.configureDebugVisualizer(p.COV_ENABLE_GUI, 0)
    p.configureDebugVisualizer(p.COV_ENABLE_RENDERING, 1)
    # SHortcurs removed, sot that keyboard can be used to drive a plane.
    p.configureDebugVisualizer(p.COV_ENABLE_KEYBOARD_SHORTCUTS, 0)
    #p.configureDebugVisualizer(p.COV_ENABLE_PLANAR_REFLECTION, 1)

    p.configureDebugVisualizer(p.COV_ENABLE_GUI, 0)
    p.setGravity(0, 0, -self.gravity)
    self.hawk = HawkFactory.create_vehicle(env_vars)
    self.no_linear_damping(self.hawk.vehicle_id)
    self._initilaizing_first_records()

  def _initilaizing_first_records(self):
    initilaizing_first_records = True
    while initilaizing_first_records:
      done, is_set = self._step_simulation()
      initilaizing_first_records = False == is_set
      p.stepSimulation()

  def reset(self):
    # TODO: voisin ehkä käyttää p.restoreState?
    p.removeBody(self.hawk.vehicle_id)
    self.hawk = HawkFactory.create_vehicle(self.env_vars)
    self.p_simulation_steps = 0
    self._initilaizing_first_records()
    
    state = self.hawk.get_state()
    return state

  
  def no_linear_damping(self, obj_id):
    p.changeDynamics(obj_id, -1, linearDamping = 0.0)
    tot_mass = 0
    for link_id in range(-1, p.getNumJoints(obj_id)):
      tot_mass += p.getDynamicsInfo(obj_id, link_id)[0]
      p.changeDynamics(obj_id, link_id, linearDamping = 0.0)
    
    print("Total mass:", tot_mass, "kg")

  def _step_simulation(self):
    self.hawk._force_to_motor()

    self.hawk.randome_forces.render()
    is_set = self.hawk.update_position_info()

    return self._is_done(), is_set

  def _is_done(self):
    if self.p_simulation_steps > self.env_vars.steps_per_episode:
      return True

    pos, pitch,roll,yaw,speed = self.hawk.state_info()
    x,y,z = pos
    return False

  def _get_acceleration_penalty(self):
    z, xy = self._get_z_and_xy_penalties()
    return z - xy**2

  def _get_fuselag_2_3_ecceleration_diff_penalty(self):
    ts = 30
    fuselag2 = self.hawk.non_grav_acce("fuselag2", ts)
    fuselag2_right = self.hawk.non_grav_acce("fuselag2_right", ts)
    to_right_penalty = np.absolute(fuselag2_right - fuselag2).sum()*8
    
    return -(to_right_penalty)

  def _get_z_and_xy_penalties(self):
    x,y,z = self.hawk.non_gravity_acceleration()
    # target z == 1 is to win gravity
    x_tar,y_tar,z_tar = 0,0,0.1
    dxdy = np.absolute([x_tar-x,y_tar-y]).sum()
    dz = z - z_tar
    reward = dz*abs(dz)

    return reward, dxdy

  def is_full_render(self):
    # full render x times per second
    full_render_every = self.steps_per_sec // 80
    return self.env_step_count % full_render_every == 0

  def list_keyboard_actions(self):
    if not self.headless:
      parced = KeyboardActions.list_keyboard_actions()
      return parced
    else:
      # TODO: generoi actionit
      return None

  def physics_simulation(self, action):
    self.hawk.presimulation_step(action, self.is_full_render())

    done = self._is_done()
    for _ in range(self.simulations_per_step):
      if done:
        state = self.reset()#.tolist()
        return True, state
        
      done, is_set = self._step_simulation() # Motor forces must be set before each simulation step. 
      p.stepSimulation()

    state = self.hawk.get_state()
    return done, state

  # TODO: Now returns dict. Maybe more performant if returns array of values. Then get_action_meaning -function, would tell which filed is what.
  def step(self, action):
    if self.p_simulation_steps > self.episode_length_s*self.steps_per_sec:
      raise Exception("Episode has ended")

    self.env_step_count += 1
    self.p_simulation_steps += 1
    
    done, state = self.physics_simulation(action)

    if not self.headless:
      self.move_camera(self.env_step_count==1)
      time.sleep(0.02)

    keyboard_actions = self.list_keyboard_actions()
    # TODO: Either state array should be dict or vehicle_state should be included to state array.
    return state, done, {"user_inputs": keyboard_actions}

  def move_camera(self, first_time):
    pos, pitch,roll,yaw,speed = self.hawk.state_info()
    outp = p.getDebugVisualizerCamera()
    yaw = outp[8]
    pitch = outp[9]
    dist = outp[10]
    if first_time:
      dist = 1
    x,y,z = pos
    
    p.resetDebugVisualizerCamera(dist,yaw,pitch,(x,y,z+0.3))
    p.removeAllUserDebugItems()
    
    x_str = str(np.round(x, 2))
    y_str = str(np.round(y, 2))
    z_str = str(np.round(z, 2))
    speed_str = str(np.round(speed, 2))
    f = self.hawk.motor_state.targets
    ff_str = str(np.round(f.front,2))
    fbl_str = str(np.round(f.back_left,2))
    fbr_str = str(np.round(f.back_right,2))

    text = f"height: {z_str}, speed:{speed_str}, Ff: {ff_str}, Fbl: {fbl_str}, Fbr: {fbr_str}"
    
    p.addUserDebugText(text, [x,y,z+0.7], [0,0,0])
