from easydict import EasyDict
import time
import numpy as np
import torch
from models.ppo.data_converter import PpoDataConverter
from models.modes import Modes


class Simulator:
  def __init__(self, envs, env_vars, agent, degub=False):
    self.degub = degub
    self.envs = envs
    self.env_vars = env_vars
    self.info = env_vars.info 
    self.agent = agent

    self.writer = env_vars.writer

  def collect_episode_data(self, num_steps):
    #TODO: Voisko experimentin keräämisen laittaa omaan luokkaan. Jos ei ole tarin niin sitten ei keräisi experimenttiä
    log_probs = []
    values    = []
    states    = []
    actions   = []
    rewards   = []
    masks     = []
    locs = []
    scales = []
    entropy = 0

    state = self.envs.reset()
    counter = 0
    # TODO: if different envs done different times then all dones will reset randomnes
    rnd_steps_counter = 0
    #stabilizer_step_counter = 0

    random_steps_at_begin = self.env_vars.random_steps_at_begin

    user_inputs = None
    while len(actions) < num_steps:
      counter += 1
      
      if counter > 30*num_steps:
        raise Exception("Something weird happening. Too many loops")
      
      # prediction_info = {"train_step", "reset_episode"}
      (dist, value), prediction_info, state = self.agent.prediction_model(state, user_inputs)

      # Multi phase training is finished
      if prediction_info.reset_episode and self.env_vars.train:
        self.envs.reset()
        self.agent.reset_episode()
        continue

      action = dist.sample()

      if rnd_steps_counter < random_steps_at_begin:
        action = torch.rand(*action.shape)
        action = action*2 - 1

      if torch.isnan(action).sum(): 
        raise Exception("action contains nan")

      # TODO:  pitääkö lisätä too_extreme_actions_penalty?
      # too_extreme_actions_penalty = -max(descaling_error - 1.5, 0)
      action_dict, descaling_error = self.agent.model_actions_to_motor_commands(action, user_inputs, state)
      
      #print(action_dict, "action_dictgjtgjtugu")
      next_state, done, step_info = self.envs.step(action_dict)


      user_inputs = [a["user_inputs"] for a in list(step_info)]
      self.agent.change_mode(user_inputs)
      
      reward = self.agent.get_reward(next_state, state, step_info)
      reward -= descaling_error
        
      if done.sum() > 0:
        rnd_steps_counter = 0
        #stabilizer_step_counter = 0
        random_steps_at_begin = self.env_vars.random_steps_at_begin
        #self.agent.set_model(self.env_vars.default_mode, new_episode = True)
        self.agent.reset_episode()

      elif rnd_steps_counter < random_steps_at_begin:
        rnd_steps_counter += 1
        continue

      elif not prediction_info.train_step:
        state = next_state
        continue
      else:
        locs.append(dist.loc.cpu().detach())
        scales.append(dist.scale.cpu().detach())

        log_prob = dist.log_prob(action)
        entropy += dist.entropy().mean()

        log_probs.append(log_prob)
        values.append(value)

        rewards.append(reward)
        masks.append(1.0 - done)

        states.append(state.tolist()) 
        actions.append(action)
        state = next_state


    (dist, next_value), prediction_info, next_state = self.agent.prediction_model(next_state, user_inputs)

    action_dist = EasyDict({"loc": locs, "scale": scales})

    values.append(next_value)
    rew_avg = np.array(rewards).mean()
    
    episode_info = EasyDict({"rew_avg": rew_avg,"reward_std": np.array(rewards).std(), "log_probs":log_probs, \
      "values":values ,"states":states ,"actions":actions ,"masks":masks, "entropy":entropy, "rewards":rewards, \
      "action_distribution":action_dist})
    return  episode_info

  def run(self):
    frame_idx = 0
    early_stop = False
    rew_avg = 0

    
    #raise Exception(self.agent.current_mode, self.env_vars.default_mode)

    
    num_steps = self.env_vars.steps_per_episode * self.env_vars.full_episodes_cnt
    max_frames = self.env_vars.max_times_to_be_updated

    start = time.time()
    while frame_idx < max_frames and not early_stop:
      self.agent.reset_episode()
      frame_idx += 1
      step = self.agent.start_step + frame_idx

      episode_info = self.collect_episode_data(num_steps)
      if self.env_vars.train:
        mean_loss, returns = self.agent.update(episode_info, step)
        self.writer.add_scalar('reward', episode_info["rew_avg"], step)
        self.writer.add_scalar('reward_std', episode_info["reward_std"], step) 
        ret = returns.cpu().detach()
        self.writer.add_scalar('return', ret.mean().item(), step) 
        self.writer.add_scalar('return_std', ret.std().item(), step)
        self.writer.add_scalar('train loss', mean_loss, step)
    
      rew_avg += episode_info["rew_avg"]
      save_every_nth = max_frames
      if frame_idx % save_every_nth == 0:
        rew_avg = rew_avg/save_every_nth
        self.agent.save_scheduled_model(rew_avg, step)
        print("Avg reward", rew_avg, ". Sum of rewards", frame_idx)
        rew_avg = 0

      if frame_idx == 1:
        duration = time.time() - start
        print("First iteration: Avg reward", rew_avg, " frame idx", frame_idx, ", duration ", duration)

    model_state_summary = self.agent.config_summary()
    summary = model_state_summary + f", actions per episode {num_steps}, times model updated {max_frames}"
    self.writer.add_text('simulator.py', summary, self.env_vars.run_number)
  
